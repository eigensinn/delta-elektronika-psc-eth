from rest_framework import serializers
from psceth.models import PscEthSaver, ParametersControl, CalibrationControl, SequencerControl, RegisterControl, \
    SettingsControl, PscEthBase


class PscEthBaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = PscEthBase
        fields = ('idn', 'pud', 'last_error', 'pwd')


class PscEthSaverSerializer(serializers.ModelSerializer):
    parameters = serializers.JSONField()
    calibration = serializers.JSONField()
    sequencer = serializers.JSONField()
    register = serializers.JSONField()
    settings = serializers.JSONField()

    class Meta:
        model = PscEthSaver
        fields = ('saver_name', 'parameters', 'calibration', 'sequencer', 'register', 'settings')


class ParametersControlSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParametersControl
        fields = ('type', 'output_voltage', 'output_current', 'max_voltage', 'max_current')


class CalibrationControlSerializer(serializers.ModelSerializer):
    class Meta:
        model = CalibrationControl
        fields = ('type', 'gain_source_voltage', 'gain_source_current', 'offset_source_voltage',
                  'offset_source_current', 'gain_measure_voltage', 'gain_measure_current', 'offset_measure_voltage',
                  'offset_measure_current')


class CatalogSerializer(serializers.ModelSerializer):
    seq_body = serializers.JSONField()

    class Meta:
        model = SequencerControl
        fields = ('seq_name', 'seq_body', 'step_number', 'step_body')


class SequencerControlSerializer(serializers.ModelSerializer):
    seq_for_upload = serializers.JSONField()

    class Meta:
        model = SequencerControl
        fields = ('type', 'selected_sequence', 'seq_for_upload')


class RegisterControlSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegisterControl
        fields = ('type', 'event_status_enable', 'service_request_enable', 'positive_transition_register',
                  'negative_transition_register', 'enable_register')


class SettingsControlSerializer(serializers.ModelSerializer):
    class Meta:
        model = SettingsControl
        fields = ('type', 'rng_gain_source_voltage', 'rng_gain_source_current', 'rng_offset_source_voltage',
                  'rng_offset_source_current', 'rng_gain_measure_voltage', 'rng_gain_measure_current',
                  'rng_offset_measure_voltage', 'rng_offset_measure_current', 'pwd_state', 'old_pwd', 'new_pwd')
