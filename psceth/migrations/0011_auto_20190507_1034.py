# Generated by Django 2.1.7 on 2019-05-07 07:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('psceth', '0010_auto_20190506_1125'),
    ]

    operations = [
        migrations.RenameField(
            model_name='registercontrol',
            old_name='enable_register',
            new_name='enable_state',
        ),
        migrations.RenameField(
            model_name='registercontrol',
            old_name='evt_status_enable',
            new_name='event_status_enable',
        ),
        migrations.RenameField(
            model_name='registercontrol',
            old_name='service_req_enable',
            new_name='service_request_enable',
        ),
        migrations.AddField(
            model_name='registercontrol',
            name='register',
            field=models.CharField(default='user_input_registers', max_length=200),
        ),
        migrations.AlterField(
            model_name='registercontrol',
            name='type',
            field=models.CharField(default='evt_status_enable', max_length=50),
        ),
    ]
