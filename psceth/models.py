from django.db import models
from django.utils.translation import ugettext_lazy as _
from jsonfield.fields import JSONField


class PscEthBase(models.Model):
    idn = models.CharField(max_length=200, default='None', verbose_name=_('Identification Information'))
    pud = models.CharField(max_length=72, default='None', verbose_name=_('Protected User Data'))
    last_error = models.CharField(max_length=400, default='None', verbose_name=_('Last Error'))
    pwd = models.CharField(max_length=100, default='DEFAULT', verbose_name=_('Password'))


class PscEthSaver(models.Model):
    saver_name = models.CharField(max_length=200, default='None', verbose_name=_('Saver name'))
    parameters = JSONField(default='{}', verbose_name=_('parameters'))
    calibration = JSONField(default='{}', verbose_name=_('calibration'))
    sequencer = JSONField(default='{}', verbose_name=_('sequencer'))
    register = JSONField(default='{}', verbose_name=_('register'))
    settings = JSONField(default='{}', verbose_name=_('settings'))


class ParametersControl(models.Model):
    type = models.CharField(max_length=50, default='output_voltage')
    measure_voltage = models.FloatField(default=0.0)
    measure_current = models.FloatField(default=0.0)
    measure_power = models.FloatField(default=0.0)
    output_voltage = models.FloatField(default=0.0)
    output_current = models.FloatField(default=0.0)
    max_voltage = models.FloatField(default=0.0)
    max_current = models.FloatField(default=0.0)


class CalibrationControl(models.Model):
    type = models.CharField(max_length=50, default='gain_source_voltage')
    gain_source_voltage = models.FloatField(default=0.0)
    gain_source_current = models.FloatField(default=0.0)
    offset_source_voltage = models.FloatField(default=0.0)
    offset_source_current = models.FloatField(default=0.0)
    gain_measure_voltage = models.FloatField(default=0.0)
    gain_measure_current = models.FloatField(default=0.0)
    offset_measure_voltage = models.FloatField(default=0.0)
    offset_measure_current = models.FloatField(default=0.0)


class Catalog(models.Model):
    seq_name = models.CharField(max_length=200, default='None', verbose_name=_('Sequence name'))
    seq_body = JSONField(default='{}', verbose_name=_('Sequence body'))


class SequencerControl(models.Model):
    type = models.CharField(max_length=50, default='selected_sequence')
    selected_sequence = models.CharField(max_length=200, default='None')
    seq_for_upload = models.CharField(max_length=200, default='1 sv=0')


class RegisterControl(models.Model):
    type = models.CharField(max_length=50, default='event_status_enable')
    register = models.CharField(max_length=200, default='user_input_registers')
    event_status_enable = models.PositiveIntegerField(default=0)
    service_request_enable = models.PositiveSmallIntegerField(default=0)
    positive_transition_register = models.BooleanField(default=False)
    negative_transition_register = models.BooleanField(default=False)
    enable_state = models.BooleanField(default=False)
    event_status_register = models.BinaryField(default=b'')
    status_byte = models.BinaryField(default=b'')


class SettingsControl(models.Model):
    type = models.CharField(max_length=50, default='rng_gain_source_voltage')
    rng_output_voltage = models.IntegerField(default=100)
    rng_output_current = models.IntegerField(default=100)
    rng_max_voltage = models.IntegerField(default=100)
    rng_max_current = models.IntegerField(default=100)
    rng_gain_source_voltage = models.IntegerField(default=100)
    rng_gain_source_current = models.IntegerField(default=100)
    rng_offset_source_voltage = models.IntegerField(default=100)
    rng_offset_source_current = models.IntegerField(default=100)
    rng_gain_measure_voltage = models.IntegerField(default=100)
    rng_gain_measure_current = models.IntegerField(default=100)
    rng_offset_measure_voltage = models.IntegerField(default=100)
    rng_offset_measure_current = models.IntegerField(default=100)
    pwd_state = models.BooleanField(default=False)
    old_pwd = models.CharField(max_length=100, default='DEFAULT', verbose_name=_('Password'))
    new_pwd = models.CharField(max_length=100, default='NEW_PWD', verbose_name=_('Password'))
