from psceth.models import PscEthSaver, ParametersControl, CalibrationControl, Catalog, SequencerControl,\
    RegisterControl, SettingsControl, PscEthBase
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext as _
from socket import socket, timeout, AF_INET, SOCK_STREAM, IPPROTO_TCP
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from django.core import serializers as django_serializers
from psceth.serializers import PscEthSaverSerializer, ParametersControlSerializer, CalibrationControlSerializer,\
    CatalogSerializer, SequencerControlSerializer, RegisterControlSerializer, SettingsControlSerializer,\
    PscEthBaseSerializer


tcp_ip = '10.1.0.101'
tcp_port = 8462
buffer_size = 1024


def send_to_psceth(message):
    s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)
    s.connect((tcp_ip, tcp_port))
    s.send(message)
    s.settimeout(1)
    try:
        data = s.recv(buffer_size)
    except timeout:
        s.close()
        return False
    s.close()
    return data


class RegisterControlViewSet(viewsets.ModelViewSet):
    queryset = RegisterControl.objects.all()
    serializer_class = RegisterControlSerializer

    reg = {'user_input_registers': 'UINPut:', 'status_operation_registers': 'STATus:OPERation:',
           'status_operation_settings_registers': 'STATus:OPERation:SETTings:',
           'status_operation_regulating_registers': 'STATus:OPERation:REGUlating:',
           'status_operation_rcontrol_registers': 'STATus:OPERation:RCONtrol:',
           'status_operation_shutdown_registers': 'STATus:OPERation:SHUTdown:',
           'status_operation_shutdown_protection_registers': 'STATus:OPERation:SHUTdown:PROTection:',
           'status_questionable_registers': 'STATus:QUESTionable:',
           'status_questionable_voltage_registers': 'STATus:VOLTage:',
           'status_questionable_current_registers': 'STATus:CURRent:'}

    @action(detail=False, methods=['get'])
    def get_event_status_register(self, request):
        state = send_to_psceth('*ESR?\n')
        if state:
            try:
                p = RegisterControl.objects.get(pk=1)
            except ObjectDoesNotExist:
                p = RegisterControl(id=1, event_status_register=state)
                p.save()
            else:
                p.event_status_register = state
                p.save()
            return Response(state)
        else:
            return Response(_("Socket timeout"))

    @action(detail=False, methods=['get'])
    def get_status_byte(self, request):
        state = send_to_psceth('*STB?\n')
        if state:
            try:
                p = RegisterControl.objects.get(pk=1)
            except ObjectDoesNotExist:
                p = RegisterControl(id=1, status_byte=state)
                p.save()
            else:
                p.status_byte = state
                p.save()
            return Response(state)
        else:
            return Response(_("Socket timeout"))

    @action(detail=False, methods=['post'])
    def set_register(self, request):
        serializer = RegisterControlSerializer(data=request.data)
        if serializer.is_valid():
            typer = serializer.data['type']
            register_type = serializer.data['register']
            cmd = {'event_status_enable': '*ESE ', 'service_request_enable': '*SRE ',
                   'positive_transition_register': 'PTR ', 'negative_transition_register': 'NTR ',
                   'enable_state': 'ENABle '}
            data = serializer.data[typer]
            if cmd[typer].startswith('*'):
                state = send_to_psceth(cmd[typer] + data + '\n')
            else:
                state = send_to_psceth(self.reg[register_type] + cmd[typer] + data + '\n')
            if state:
                try:
                    rc = RegisterControl.objects.get(pk=1)
                except ObjectDoesNotExist:
                    rc = RegisterControl(id=1)
                    rc.save()
                setattr(rc, typer, serializer.data[typer])
                rc.save(update_fields=[typer])
                return Response(state)
            else:
                return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def get_register(self, request):
        serializer = RegisterControlSerializer(data=request.data)
        if serializer.is_valid():
            typer = serializer.data['type']
            register_type = serializer.data['register']
            cmd = {'event_status_enable': '*ESE?\n', 'service_request_enable': '*SRE?\n',
                   'condition': 'CONDition?\n', 'event_state': 'EVENt?\n',
                   'positive_transition_register': 'PTR?\n',
                   'negative_transition_register': 'NTR?\n', 'enable_state': 'ENABle?\n',
                   'clear_all_event_registers': '*CLS\n'}
            if cmd[typer].startswith('*'):
                state = send_to_psceth(cmd[typer])
            else:
                state = send_to_psceth(self.reg[register_type] + cmd[typer])
            if state:
                return Response(state)
            else:
                return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SettingsControlViewSet(viewsets.ModelViewSet):
    queryset = SettingsControl.objects.all()
    serializer_class = SettingsControlSerializer

    @action(detail=False, methods=['post'])
    def save_pwd(self, request):
        serializer = SettingsControlSerializer(data=request.data)
        if serializer.is_valid():
            old_pwd = serializer.data['old_pwd']
            new_pwd = serializer.data['new_pwd']
            if old_pwd in ['DEFAULT', '']:
                result = send_to_psceth('SYSTem:PASSword DEFAULT,' + new_pwd + '\n')
                if result:
                    return Response(result)
                else:
                    return Response(_("Socket timeout"))
            else:
                result = send_to_psceth('SYSTem:PASSword ' + old_pwd + ',' + new_pwd + '\n')
                if result:
                    return Response(result)
                else:
                    return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'])
    def get_pwd_status(self, request):
        state = send_to_psceth('SYSTem:PASSword:STATus?\n')
        if state:
            try:
                p = SettingsControl.objects.get(pk=1)
            except ObjectDoesNotExist:
                p = SettingsControl(id=1, pwd_state=state)
                p.save()
            else:
                p.pwd_state = state
                p.save()
            return Response(state)
        else:
            return Response(_("Socket timeout"))

    @action(detail=False, methods=['post'])
    def set_range(self, request):
        serializer = SettingsControlSerializer(data=request.data)
        if serializer.is_valid():
            typer = serializer.data['type']
            try:
                control = SettingsControl.objects.get(id=1)
            except ObjectDoesNotExist:
                c = SettingsControl(id=1)
                c.save()
                control = SettingsControl.objects.get(id=1)
            setattr(control, typer, serializer.data[typer])
            control.save(update_fields=[typer])
            return Response(serializer.data[typer])
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'])
    def get_ranges(self, request):
        serializer = SettingsControlSerializer(data=request.data)
        if serializer.is_valid():
            try:
                control = SettingsControl.objects.get(id=1)
            except ObjectDoesNotExist:
                c = SettingsControl(id=1)
                c.save()
                control = SettingsControl.objects.get(id=1)
            return Response(django_serializers.serialize('json', [control]))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PscEthBaseViewSet(viewsets.ModelViewSet):
    queryset = PscEthBase.objects.all()
    serializer_class = PscEthBaseSerializer

    @action(detail=False, methods=['get'])
    def get_idn(self, request):
        value = send_to_psceth('*IDN?\n')
        if value:
            try:
                p = PscEthBase.objects.get(pk=1)
            except ObjectDoesNotExist:
                p = PscEthBase(id=1, idn=value)
                p.save()
            else:
                p.idn = value
                p.save()
            return Response(value)
        else:
            return Response(_("Socket timeout"))

    @action(detail=False, methods=['post'])
    def save_all(self, request):
        serializer = PscEthBaseSerializer(data=request.data)
        if serializer.is_valid():
            pwd_state = send_to_psceth('SYSTem:PASSword:STATus?\n')
            if pwd_state:
                if pwd_state == 0:
                    if send_to_psceth('*SAV\n'):
                        return Response(_("Saved"))
                    else:
                        return Response(_("Socket timeout"))
                elif pwd_state == 1:
                    pwd = serializer.data['pwd']
                    if pwd == '':
                        return Response(_("Please enter a password"))
                    else:
                        result = send_to_psceth('*SAV '+pwd+'\n')
                        if result:
                            return Response(result)
                        else:
                            return Response(_("Socket timeout"))
            else:
                return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'])
    def get_last_error(self, request):
        value = send_to_psceth('SYSTem:ERRor?\n')
        if value:
            try:
                p = PscEthBase.objects.get(pk=1)
            except ObjectDoesNotExist:
                p = PscEthBase(id=1, last_error=value)
                p.save()
            else:
                p.last_error = value
                p.save()
            return Response(value)
        else:
            return Response(_("Socket timeout"))

    @action(detail=False, methods=['get'])
    def get_pud(self, request):
        pud = send_to_psceth('*PUD?\n')
        if pud:
            try:
                PscEthBase.objects.get(pk=1)
            except ObjectDoesNotExist:
                p = PscEthBase(id=1, pud=pud)
                p.save()
            return Response(pud)
        else:
            return Response(_("Socket timeout"))

    @action(detail=False, methods=['post'])
    def set_pud(self, request):
        serializer = PscEthBaseSerializer(data=request.data)
        if serializer.is_valid():
            pud = serializer.data['pud']
            if send_to_psceth('*PUD ' + pud + '\n'):
                try:
                    p = PscEthBase.objects.get(pk=1)
                except ObjectDoesNotExist:
                    p = PscEthBase(id=1)
                    p.save()
                p.pud = pud
                p.save(update_fields=['pud'])
                return Response(pud)
            else:
                return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ParametersControlViewSet(viewsets.ModelViewSet):
    queryset = ParametersControl.objects.all()
    serializer_class = ParametersControlSerializer

    names = {'measure-voltage': 'VOLTage', 'measure-current': 'CURRent', 'measure-power': 'POWer',
             'output-voltage': 'VOLTage', 'output-current': 'CURRent', 'max-voltage': 'VOLTage:MAXimum',
             'max-current': 'CURRent:MAXimum'}

    @action(detail=False, methods=['post'])
    def measure(self, request):
        """ Request options:
        MEASure:VOLTage?\n
        MEASure:CURRent?\n
        MEASure:POWer?\n
        """
        serializer = ParametersControlSerializer(data=request.data)
        if serializer.is_valid():
            value = send_to_psceth('MEASure:' + self.names[request.data["type"]] + '?\n')
            if value:
                return Response(value)
            else:
                return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def get_source(self, request):
        """ Request options:
        SOURce:VOLTage:MAXimum?\n
        SOURce:CURRent:MAXimum?\n
        SOURce:VOLTage?\n
        SOURce:CURRent?\n
        """
        serializer = ParametersControlSerializer(data=request.data)
        if serializer.is_valid():
            typer = serializer.data['type']
            value = send_to_psceth('SOURce:'+self.names[typer]+'?\n')
            if value:
                return Response(value)
            else:
                return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def set_source(self, request):
        """ Request options:
        SOURce:VOLTage:MAXimum <value>\n
        SOURce:CURRent:MAXimum <value>\n
        SOURce:VOLTage <value>\n
        SOURce:CURRent <value>\n
        """
        serializer = ParametersControlSerializer(data=request.data)
        if serializer.is_valid():
            typer = serializer.data['type']
            value = serializer.data[typer]
            if send_to_psceth('SOURce:' + self.names[typer] + ' ' + value + '\n'):
                return Response(value)
            else:
                return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CalibrationControlViewSet(viewsets.ModelViewSet):
    queryset = CalibrationControl.objects.all()
    serializer_class = CalibrationControlSerializer

    names = {0: 'gain-source-current', 1: 'offset-source-current', 2: 'gain-source-voltage',
             3: 'offset-source-voltage', 4: 'gain-measure-current', 5: 'gain-measure-voltage',
             6: 'offset-measure-current', 7: 'offset-measure-voltage'}

    @action(detail=False, methods=['get'])
    def get_calibration(self, request):
        """try:
            CalibrationControl.objects.get(id=1)
        except ObjectDoesNotExist:
            c = CalibrationControl(id=1)
            c.save()"""
        resp = {}
        for i in range(8):
            """state = send_to_psceth('CAL ' + str(i) + '?\n')
            if state:
                print(0)
                resp[self.names[i]] = state
            else:
                resp[self.names[i]] = 0"""
            resp[self.names[i]] = 0
        return Response(resp)

    @action(detail=False, methods=['post'])
    def calibrate(self, request):
        serializer = CalibrationControlSerializer(data=request.data)
        if serializer.is_valid():
            typer = serializer.data['type']
            for key, value in self.names.items():
                if value == typer:
                    if send_to_psceth('CAL ' + key + ',' + serializer.data[typer] + '\n'):
                        return Response(serializer.data[typer])
                    else:
                        return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SequencerControlViewSet(viewsets.ModelViewSet):
    queryset = SequencerControl.objects.all()
    serializer_class = SequencerControlSerializer

    @action(detail=False, methods=['get'])
    def get_selected_sequence_name(self, request):
        sel_seq = send_to_psceth('PROGram:SELected:NAME?\n')
        if sel_seq:
            if sel_seq == '\n':
                return Response(_("No sequence is selected"))
            else:
                c = SequencerControl(id=1)
                c.selected_sequence = sel_seq
                c.save(update_fields=['selected_sequence'])
                return Response(sel_seq)
        else:
            return Response(_("Socket timeout"))

    @action(detail=False, methods=['get'])
    def get_sequencer_data(self, request):
        try:
            seq = SequencerControl.objects.get(id=1)
        except ObjectDoesNotExist:
            seq = SequencerControl(id=1)
            seq.save()
        return Response({'selected_sequence': seq.selected_sequence, 'seq_for_upload': seq.seq_for_upload})

    @action(detail=False, methods=['get'])
    def trigger_a_step(self, request):
        if send_to_psceth('TRIGger:IMMediate\n'):
            return Response(_("The sequence is proceed"))
        else:
            return Response(_("Socket timeout"))

    @action(detail=False, methods=['get'])
    def read_sequence_mode(self, request):
        """ Responses options:
        STOP\n
        PAUSE,<step>\n
        RUN,<step>\n
        """
        seq_mode = send_to_psceth('PROGram:SELected:STATe?\n')
        if seq_mode:
            return Response(seq_mode)
        else:
            return Response(_("Socket timeout"))

    @action(detail=False, methods=['post'])
    def set_sequence_mode(self, request):
        serializer = SequencerControlSerializer(data=request.data)
        if serializer.is_valid():
            """ Request options:
            PROGram:SELected:STATe RUN\n
            PROGram:SELected:STATe PAUSe\n
            PROGram:SELected:STATe CONTinue\n
            PROGram:SELected:STATe NEXT\n
            PROGram:SELected:STATe STOP\n
            """
            seq_mode = serializer.data['type']
            if send_to_psceth('PROGram:SELected:STATe ' + seq_mode + '\n'):
                return Response(seq_mode)
            else:
                return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CatalogViewSet(viewsets.ModelViewSet):
    queryset = Catalog.objects.all()
    serializer_class = CatalogSerializer

    @action(detail=False, methods=['post'])
    def upload_sequence(self, request):
        serializer = CatalogSerializer(data=request.data)
        if serializer.is_valid():
            seq_name = serializer.data['seq_name']
            try:
                Catalog.objects.get(seq_name=seq_name)
            except ObjectDoesNotExist:
                return Response(_("Sequence `%(name)s` does not exist") % {'name': seq_name},
                                status=status.HTTP_404_NOT_FOUND)
            else:
                sb = serializer.data['step_body']
                sb_pair = sb.split(' ', 1)
                # example: 'PROGram:SELected:STEP ' + 21 + ' ' + 'CJNE #A,3,15' + '\n'
                if send_to_psceth('PROGram:SELected:STEP ' + sb_pair[0] + ' ' + sb_pair[1] + '\n'):
                    c = SequencerControl(id=1)
                    c.seq_for_upload = sb
                    c.save()
                    return Response(sb)
                else:
                    return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def download_sequence(self, request):
        serializer = CatalogSerializer(data=request.data)
        if serializer.is_valid():
            seq_name = serializer.data['seq_name']
            try:
                Catalog.objects.get(saver_name=seq_name)
            except ObjectDoesNotExist:
                return Response(_("Sequence `%(name)s` does not exist") % {'name': seq_name},
                                status=status.HTTP_404_NOT_FOUND)
            else:
                # example: 'PROGram:SELected:STEP ' + '1' + '?\n'
                step_body = send_to_psceth('PROGram:SELected:STEP ' + serializer.data['step_number'] + '?\n')
                if step_body:
                    return Response(step_body)
                else:
                    return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def create_sequence(self, request):
        serializer = CatalogSerializer(data=request.data)
        if serializer.is_valid():
            seq_name = serializer.data['seq_name']
            try:
                Catalog.objects.get(seq_name=seq_name)
            except ObjectDoesNotExist:
                if send_to_psceth('PROGram:SELected:NAME ' + seq_name + '\n'):
                    s = Catalog(seq_name=seq_name)
                    s.save()
                    return Response(_("Sequence `%(name)s` has been created") % {'name': seq_name})
                else:
                    return Response(_("Socket timeout"))
            else:
                if send_to_psceth('PROGram:SELected:NAME ' + seq_name + '\n'):
                    return Response(seq_name)
                else:
                    return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def del_sequence(self, request):
        serializer = CatalogSerializer(data=request.data)
        if serializer.is_valid():
            seq_name = serializer.data['seq_name']
            try:
                Catalog(seq_name=seq_name)
            except ObjectDoesNotExist:
                return Response(_("Sequence name `%(name)s` is already do not exist") % {'name': seq_name},
                                status=status.HTTP_404_NOT_FOUND)
            else:
                if send_to_psceth('PROGram:SELected:NAME ' + seq_name + ';PROGram:SELected:DELete\n'):
                    Catalog.objects.get(saver_name=seq_name).delete()
                    return Response(_("Sequence `%(name)s` is removed") % {'name': seq_name})
                else:
                    return Response(_("Socket timeout"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'])
    def get_catalog(self, request):
        cat = send_to_psceth('PROGram:CATalog?\n')
        # response example: 'WAWE1\nPROCESS4\nRAMP-UP\n\n'
        if cat:
            cat_list = cat.replace('\n\n', '').split('\n')
            for seq in cat_list:
                Catalog.objects.get_or_create(seq_name=seq)
            return Response(cat_list)
        else:
            return Response(_("Socket timeout"))

    @action(detail=False, methods=['get'])
    def del_catalog(self, request):
        try:
            c = Catalog.objects.objects.all()
        except ObjectDoesNotExist:
            return Response(_("Catalog already does not exist"), status=status.HTTP_404_NOT_FOUND)
        else:
            if send_to_psceth('PROGram:CATalog:DELete\n'):
                c.delete()
                return Response(_("Catalog is removed"))
            else:
                return Response(_("Socket timeout"))


class PscEthSaverViewSet(viewsets.ModelViewSet):
    queryset = PscEthSaver.objects.all()
    serializer_class = PscEthSaverSerializer

    @action(detail=False, methods=['post'])
    def save_preset(self, request):
        serializer = PscEthSaverSerializer(data=request.data)
        if serializer.is_valid():
            saver_name = serializer.data['saver_name']
            try:
                PscEthSaver.objects.get(saver_name=saver_name)
            except ObjectDoesNotExist:
                par = ParametersControl.objects.get(id=1)
                cal = CalibrationControl.objects.get(id=1)
                seq = SequencerControl.objects.get(id=1)
                reg = RegisterControl.objects.get(id=1)
                stg = SettingsControl.objects.get(id=1)
                s = PscEthSaver(saver_name=saver_name,
                                preset=django_serializers.serialize('json', [par, cal, seq, reg, stg]))
                s.save()
                return Response(saver_name, status=status.HTTP_201_CREATED)
            else:
                return Response(_("Saver name `%(name)s` is already in use") % {'name': saver_name},
                                status=status.HTTP_403_FORBIDDEN)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def del_preset(self, request):
        serializer = PscEthSaverSerializer(data=request.data)
        if serializer.is_valid():
            saver_name = serializer.data['saver_name']
            try:
                PscEthSaver.objects.get(saver_name=saver_name).delete()
                preset_list = PscEthSaver.objects.values_list("saver_name", flat=True)
                return Response(preset_list)
            except ObjectDoesNotExist:
                return Response(_("Saver name `%(name)s` is already do not exist") % {'name': saver_name},
                                status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'])
    def get_preset_list(self):
        preset_list = PscEthSaver.objects.values_list("saver_name", flat=True)
        return Response(preset_list)
