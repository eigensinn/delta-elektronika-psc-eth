from rest_framework import routers
from .views import PscEthSaverViewSet, CatalogViewSet, SequencerControlViewSet, CalibrationControlViewSet,\
    ParametersControlViewSet, PscEthBaseViewSet, SettingsControlViewSet, RegisterControlViewSet


router = routers.DefaultRouter()
router.register(r'api/v1/preset', PscEthSaverViewSet)
router.register(r'api/v1/catalog', CatalogViewSet)
router.register(r'api/v1/sequencer', SequencerControlViewSet)
router.register(r'api/v1/calibration', CalibrationControlViewSet)
router.register(r'api/v1/parameters', ParametersControlViewSet)
router.register(r'api/v1/settings', SettingsControlViewSet)
router.register(r'api/v1/register', RegisterControlViewSet)
router.register(r'api/v1/base', PscEthBaseViewSet)

urlpatterns = router.urls
