import * as types from '../constants/ActionTypes';

export function saveSaverSuccess (saver) {
  return {
    type: types.SAVE_SAVER,
    saver
  };
}

export function loadSaverSuccess (fields) {
  return {
    type: types.LOAD_SAVER,
    fields
  };
}

export function getSaversSuccess (savers) {
  return {
    type: types.GET_SAVERS,
    savers
  };
}

export function createExpSuccess (expName) {
  return {
    type: types.CREATE_EXP,
    expName
  };
}

export function getExperimentsSuccess (experiments) {
  return {
    type: types.GET_EXPERIMENTS,
    experiments
  };
}

export function getExperimentSuccess (expData) {
  return {
    type: types.GET_EXPERIMENT,
    expData
  };
}

export function getResultSuccess (resultData) {
  return {
    type: types.GET_RESULT,
    resultData
  };
}

export function setControlSuccess (jsonstring, response) {
  const dict = { on_off: types.ON_OFF_CONTROL, sense: types.SENSE_CONTROL,
    loc_rem: types.LOC_REM_CONTROL, port: types.PORT_CONTROL,
    baudrate: types.BAUD_CONTROL, mode: types.MODE_CONTROL,
    cc_current: types.CC_CURRENT_CONTROL, cv_voltage: types.CV_VOLTAGE_CONTROL,
    cw_power: types.CW_POWER_CONTROL, cr_resistance: types.CR_RESISTANCE_CONTROL,
    max_current: types.MAX_CURRENT_CONTROL, max_voltage: types.MAX_VOLTAGE_CONTROL,
    max_power: types.MAX_POWER_CONTROL, power_density: types.POWER_DENSITY_CONTROL,
    dim_power: types.DIM_POWER, area_of_cell: types.AREA_CONTROL,
    dim_area: types.DIM_AREA, min_value: types.MIN_VALUE_CONTROL,
    factor: types.FACTOR_CONTROL, quantity_of_dots: types.QUANTITY_CONTROL,
    time_sleep: types.TIME_SLEEP_CONTROL };
  return {
    type: dict[jsonstring.type],
    meta: jsonstring.type,
    response
  };
}

export function getControlsSuccess (fields) {
  return {
    type: types.GET_CONTROLS,
    fields
  };
}

export function getPudSuccess (pud) {
  return {
    type: types.GET_PUD,
    pud
  };
}

export function setPudSuccess (pud) {
  return {
    type: types.SET_PUD,
    pud
  };
}

export function getCalibrationSuccess (json) {
  return {
    type: types.GET_CALIBRATION,
    json
  };
}

export function calibrateSuccess (calType, response) {
  return {
    type: types.CALIBRATE,
    meta: calType,
    response
  };
}

export function measureSuccess (type, value) {
  return {
    type: types.MEASURE,
    meta: type,
    value
  };
}

export function getSourceSuccess (type, value) {
  return {
    type: types.GET_SOURCE,
    meta: type,
    value
  };
}

export function setSourceSuccess (type, value) {
  return {
    type: types.SET_SOURCE,
    meta: type,
    value
  };
}

export function savePresetSuccess (presetName) {
  return {
    type: types.SAVE_PRESET,
    presetName
  };
}

export function delPresetSuccess (presets) {
  return {
    type: types.DEL_PRESET,
    presets
  };
}

export function getPresetListSuccess (presets) {
  return {
    type: types.GET_PRESET_LIST,
    presets
  };
}

export function getPwdStatusSuccess (pwdStatus) {
  return {
    type: types.PWD_STATUS,
    pwdStatus
  };
}

export function setRangeSuccess (type, value) {
  return {
    type: types.SET_RANGE,
    meta: type,
    value
  };
}

export function getRangesSuccess (jsonstring) {
  return {
    type: types.GET_RANGES,
    jsonstring
  };
}

export function getSelectedSequenceNameSuccess (selected) {
  return {
    type: types.GET_SELECTED_SEQUENCE_NAME,
    selected
  };
}

export function getSequencerDataSuccess (json) {
  return {
    type: types.GET_SEQUENCER_DATA,
    json
  };
}

export function readSequenceModeSuccess (mode) {
  return {
    type: types.READ_SEQUENCE_MODE,
    mode
  };
}

export function setSequenceModeSuccess (mode) {
  return {
    type: types.SET_SEQUENCE_MODE,
    mode
  };
}

export function uploadSequenceSuccess (stepBody) {
  return {
    type: types.UPLOAD_SEQUENCE,
    stepBody
  };
}

export function downloadSequenceSuccess (downloadedSeq) {
  return {
    type: types.DOWNLOAD_SEQUENCE,
    downloadedSeq
  };
}

export function createSequenceSuccess (selected) {
  return {
    type: types.CREATE_SEQUENCE,
    selected
  };
}

export function getCatalogSuccess (seqList) {
  return {
    type: types.GET_CATALOG,
    seqList
  };
}

export function getEventStatusRegisterSuccess (evtStatus) {
  return {
    type: types.GET_EVENT_STATUS_REGISTER,
    evtStatus
  };
}

export function getStatusByteSuccess (statusByte) {
  return {
    type: types.GET_STATUS_BYTE,
    statusByte
  };
}

export function setRegisterSuccess (register, value) {
  return {
    type: types.SET_REGISTER,
    meta: register,
    value
  };
}

export function getRegisterSuccess (register, value) {
  return {
    type: types.GET_REGISTER,
    meta: register,
    value
  };
}
