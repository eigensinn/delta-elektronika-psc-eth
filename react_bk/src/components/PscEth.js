import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import PscEthParameters from './PscEthParameters';
import PscEthCalibration from './PscEthCalibration';
import PscEthSequenser from './PscEthSequenser';
import PscEthRegister from './PscEthRegister';
import PscEthSettings from './PscEthSettings';
import { getPud, setPud } from '../api/pscethBase';

function TabContainer({ children, dir }) {
  return (
    <Typography component='div' dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: 1280,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  card: {
    minWidth: 275,
  },
});

class PscEth extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      pudValue: props.pud
    };
  }
  
  componentWillReceiveProps (nextProps) {
    const { pud } = nextProps;
    this.setState({
      pudValue: pud
    });
  }
  
  handlePudChange = event => {
    this.setState({ pudValue: event.target.value });
  };
  
  setPud = event => {
    setPud({ 'pud': event.target.value });
  }
  
  getPud = () => {
    getPud();
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  render() {
    const { classes, theme } = this.props;
    const { value, pudValue } = this.state;

    return (
      <div className={ classes.root }>
        <Card className={classes.card}>
          <CardContent>
            <TextField
              id="informer"
              label="Informer"
              multiline
              rows="6"
              className={ classes.textField }
              value="2"
              onChange={() => {}}
              margin="normal"
            />
            <TextField
              id="terminal"
              label="Terminal"
              multiline
              rows="3"
              className={ classes.textField }
              value="2"
              onChange={() => {}}
              margin="normal"
            />
          </CardContent>
          <CardActions>
            <Tooltip title="Get Identification Information" placement="top-start">
              <Button size="small" color="primary" variant="outlined">IDN?</Button>
            </Tooltip>
            <Tooltip title="Protected User Data" placement="top">
              <Button
                size="small"
                color="primary"
                variant="outlined"
                onClick={ this.getPud }
              >PUD?</Button>
            </Tooltip>
            <TextField
              id="pud"
              label="PUD"
              className={ classes.textField }
              onChange={ this.handlePudChange }
              onDoubleClick={ this.setPud }
              value={ pudValue }
              margin="normal"
            />
            <Tooltip title="Get Last Error" placement="top">
              <Button size="small" color="primary" variant="outlined">Error?</Button>
            </Tooltip>
            <Tooltip title="Save All Settings" placement="top">
              <Button size="small" color="primary" variant="outlined">Save</Button>
            </Tooltip>
            <TextField
              id="pwd"
              label="Password"
              className={ classes.textField }
              onChange={() => {}}
              margin="normal"
            />
            <Tooltip title="Clear Terminal" placement="top-end">
              <Button size="small" color="primary" variant="outlined">Clear</Button>
            </Tooltip>
          </CardActions>
        </Card>
        <AppBar position='static' color='default'>
          <Tabs
            value={ value }
            onChange={ this.handleChange }
            indicatorColor='primary'
            textColor='primary'
            variant='fullWidth'
          >
            <Tab label='Parameters' />
            <Tab label='Calibration' />
            <Tab label='Sequencer' />
            <Tab label='Register' />
            <Tab label='Settings' />
          </Tabs>
        </AppBar>
        <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={ value }
          onChangeIndex={this.handleChangeIndex}
        >
          <TabContainer dir={ theme.direction }>
            <PscEthParameters />
          </TabContainer>
          <TabContainer dir={ theme.direction }>
            <PscEthCalibration />
          </TabContainer>
          <TabContainer dir={ theme.direction }>
            <PscEthSequenser />
          </TabContainer>
          <TabContainer dir={ theme.direction }>
            <PscEthRegister />
          </TabContainer>
          <TabContainer dir={ theme.direction }>
            <PscEthSettings />
          </TabContainer>
        </SwipeableViews>
      </div>
    );
  }
}

const themeShape = {
  breakpoints: PropTypes.object
};

PscEth.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  theme: PropTypes.shape(themeShape).isRequired,
  pud: PropTypes.string.isRequired
};

const mapStateToProps = function mapStateToProps(store) {
  return {
    pud: store.pscEthBaseState.pud
  };
};

export default connect(mapStateToProps)(withStyles(styles, { withTheme: true })(PscEth));