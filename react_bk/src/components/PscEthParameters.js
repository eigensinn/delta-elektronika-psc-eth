import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import VerticalSlider from './VerticalSlider';
import { measure, getSource, setSource } from '../api/pscethParameters';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  card: {
    minWidth: 275,
  },
  dense: {
    marginTop: 16,
  }
});

class PscEthParameters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      voltageValue: props.measureData.voltage,
      currentValue: props.measureData.current,
      powerValue: props.measureData.power,
      outputVoltageValue: props.sourceData.output_voltage,
      outputCurrentValue: props.sourceData.output_current,
      maxVoltageValue: props.sourceData.max_voltage,
      maxCurrentValue: props.sourceData.max_current
    };
  }
  
  componentWillReceiveProps (nextProps) {
    const { measureData, sourceData } = nextProps;
    this.setState({
      voltageValue: measureData.voltage,
      currentValue: measureData.current,
      powerValue: measureData.power,
      outputVoltageValue: sourceData.output_voltage,
      outputCurrentValue: sourceData.output_current,
      maxVoltageValue: sourceData.max_voltage,
      maxCurrentValue: sourceData.max_current
    });
  }
  
  handleChange = event => {
    this.setState({ [event.target.id.replace('Inp', '')]: event.target.value });
  }

  measure = event => {
    measure({ 'type': event.target.id });
  }
  
  getSource = event => {
    getSource({ 'type': event.target.id });
  };
  
  setSourceSld = event => {
    const type = event.target.id;
    this.setState({ [type]: event.target.value });
    setSource({ [event.target.name.replace('sld_', '')]: event.target.value, 'type': type });
  }
  
  setSource = event => {
    const type = event.target.name.replace('inp_', '');
    setSource({ [type]: event.target.value, 'type': type });
  }
  
  render() {
    const { classes } = this.props;
    const {
      voltageValue,
      currentValue,
      powerValue,
      outputVoltageValue,
      outputCurrentValue,
      maxVoltageValue,
      maxCurrentValue
    } = this.state;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid container spacing={ 8 }>
            <Card className={classes.card}>
              <CardContent>
                <TextField
                  id="inp_measure_voltage"
                  label="Measure Voltage?"
                  className={ classes.textField }
                  value={ voltageValue }
                  onChange={() => {}}
                  margin="normal"
                  variant="outlined"
                  InputProps={{
                    readOnly: true,
                  }}
                />
                <TextField
                  id="inp_measure_current"
                  label="Measure Current?"
                  className={ classes.textField }
                  value={ currentValue }
                  onChange={() => {}}
                  margin="normal"
                  variant="outlined"
                  InputProps={{
                    readOnly: true,
                  }}
                />
                <TextField
                  id="inp_measure_power"
                  label="Measure Power?"
                  className={ classes.textField }
                  value={ powerValue }
                  onChange={() => {}}
                  margin="normal"
                  variant="outlined"
                  InputProps={{
                    readOnly: true,
                  }}
                />
              </CardContent>
              <CardActions>
                <Button
                  id="measure_voltage"
                  size="small"
                  variant="outlined"
                  onClick={ this.measure }
                >Measure Voltage?</Button>
                <Button
                  id="measure_current"
                  size="small"
                  variant="outlined"
                  onClick={ this.measure }
                >Measure Current?</Button>
                <Button
                  id="measure_power"
                  size="small"
                  variant="outlined"
                  onClick={ this.measure }
                >Measure Power?</Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid container spacing={ 8 }>
            <Grid item xs={ 3 } sm={ 3 } md={ 3 } lg={ 3 }>
              <TextField
                id="outputVoltageValueInp"
                name="inp_output_voltage"
                label="Output Voltage?"
                className={classes.textField}
                value={ outputVoltageValue }
                onChange={ this.handleChange }
                onDoubleClick={ this.setSource }
                margin="normal"
                variant="outlined"
              />
              <Button
                id="output_voltage"
                size="small"
                variant="outlined"
                onClick={ this.getSource }
              >Output Voltage?</Button>
              <VerticalSlider
                id="outputVoltageValue"
                name="sld_output_voltage"
                value={ outputVoltageValue }
                onChange={ this.setSourceSld }
              />
            </Grid>
            <Grid item xs={ 3 } sm={ 3 } md={ 3 } lg={ 3 }>
              <TextField
                id="outputCurrentValueInp"
                name="inp_output_current"
                label="Output Current?"
                className={ classes.textField }
                value={ outputCurrentValue }
                onChange={ this.handleChange }
                onDoubleClick={ this.setSource }
                margin="normal"
                variant="outlined"
              />
              <Button
                id="output_current"
                size="small"
                variant="outlined"
                onClick={ this.getSource }
              >Output Current?</Button>
              <VerticalSlider
                id="outputCurrentValue"
                name="sld_output_current"
                value={ outputCurrentValue }
                onChange={ this.setSourceSld }
              />
            </Grid>
            <Grid item xs={ 3 } sm={ 3 } md={ 3 } lg={ 3 }>
              <TextField
                id="maxVoltageValueInp"
                name="max_voltage"
                label="Max Voltage?"
                className={classes.textField}
                value={ maxVoltageValue }
                onChange={ this.handleChange }
                onDoubleClick={ this.setSource }
                margin="normal"
                variant="outlined"
              />
              <Button
                id="max_voltage"
                size="small"
                variant="outlined"
                onClick={ this.getSource }
              >Max Voltage?</Button>
              <VerticalSlider
                id="maxVoltageValue"
                name="sld_max_voltage"
                value={ maxVoltageValue }
                onChange={ this.setSourceSld }
              />
            </Grid>
            <Grid item xs={ 3 } sm={ 3 } md={ 3 } lg={ 3 }>
              <TextField
                id="maxCurrentValueInp"
                name="inp_max_current"
                label="Max Current?"
                className={ classes.textField }
                value={ maxCurrentValue }
                onChange={ this.handleChange }
                onDoubleClick={ this.setSource }
                margin="normal"
                variant="outlined"
              />
              <Button
                id="max_current"
                size="small"
                variant="outlined"
                onClick={ this.getSource }
              >Max Current?</Button>
              <VerticalSlider
                id="maxCurrentValue"
                name="sld_max_current"
                value={ maxCurrentValue }
                onChange={ this.setSourceSld }
              />
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const measureShape = {
  votage: PropTypes.number,
  current: PropTypes.number,
  power: PropTypes.number
};

const sourceShape = {
  output_voltage: PropTypes.number,
  output_current: PropTypes.number,
  max_voltage: PropTypes.number,
  max_current: PropTypes.number
};

PscEthParameters.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  measureData: PropTypes.shape(measureShape).isRequired,
  sourceData: PropTypes.shape(sourceShape).isRequired
};

const mapStateToProps = function mapStateToProps(store) {
  return {
    measureData: store.pscEthParametersState.measureData,
    sourceData: store.pscEthParametersState.sourceData
  };
};

export default connect(mapStateToProps)(withStyles(styles, { withTheme: true })(PscEthParameters));