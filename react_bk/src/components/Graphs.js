import React from "react";
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ReactChartkick, { ScatterChart } from 'react-chartkick';
import Chart from 'chart.js';
import styled from '../css/controls.css';
import spectre from '../css/spectre.scss';
// import spectre from '../../node_modules/spectre.css/dist/spectre.min.css';
import { getExperiments, getExperiment, getResult } from '../api/experiments';

ReactChartkick.addAdapter(Chart);

class Graphs extends React.Component {
  state = {
    activetab: 6
  };
  
  componentWillMount() {
    getExperiments();
  };
    
  tabChange = (tabNumber) => {
    this.setState({
      activetab: tabNumber
    });
  };
  
  getResult = event => {
    getResult({ 'result_name': event.target.innerHTML, 'data_for_graph': '{}' });
  };
  
  getExperiment = event => {
    getExperiment({ 'solar_exp_name': event.target.innerHTML });
  };
  
  renderSubMenu (ex) {
    this.ex = ex;
    return (
      <ul className={ spectre.menu }>
        { this.ex.map((res) => {
          return <li className={ spectre['form-item'] } key={ res } title={ res }><div role="menuitem" onClick={ this.getResult } onKeyPress={() => {}} tabIndex={-1}>{ res }</div></li>;
        })
        }
      </ul>
    );
  }
  
  renderMenu (experiments) {
    const btn = [spectre.btn, spectre['btn-link'], spectre['dropdown-toggle']].join(' ');
    const menuItem = ['experiment', spectre['menu-item']].join(' ');
    return (
      <ul className={ spectre.menu }>
        { Object.entries(experiments).map((ex) => {
          return (<li className={ menuItem } key={ ex[0] } title={ ex[1] }><div role="menuitem" onClick={ this.getExperiment } onKeyPress={() => {}} tabIndex={-1}>{ ex[0] }</div>
            <div className={ spectre.dropdown }>
              <div role="menuitem" className={ btn } tabIndex="0">
                Show results <i className={ [spectre.icon, spectre['icon-caret']].join(' ') }></i>
              </div>
              { this.renderSubMenu(ex[1]) }
            </div>
          </li>
          );
        })
        }
      </ul>
    );
  };
  
  renderTab (experiments) {
    const { activetab } = this.state;
    const columnCol4 = [spectre.column, spectre['col-4']].join(' ');
    const btn = [spectre.btn, spectre['btn-link'], spectre['dropdown-toggle']].join(' ');
    if (activetab === 6) {
      return (
        <div className={ spectre.columns }>
          <div className={ columnCol4 }>
            <div className={ spectre['form-group'] }>
              <label className={ spectre['form-label'] } htmlFor="solar_exp_name">
                Experiment name
                <input className={ spectre['form-input'] } id="solar_exp_name" size="15" type="text" onChange={ this.createExp } />
              </label>
            </div>
          </div>
          <div className={ columnCol4 }>
            <div className={ spectre['form-group'] }>
              <label className={ spectre['form-label'] } htmlFor="dropdown">
              Show chart
                <div className={ spectre.dropdown } id="dropdown">
                  <div role="menuitem" className={ btn } tabIndex="0">
                    Show experiment <i className={ [spectre.icon, spectre['icon-caret']].join(' ') }></i>
                  </div>
                  { this.renderMenu(experiments) }
                </div>
              </label>
            </div>
          </div>
          <div className={ columnCol4 }>
            <div className={ spectre['form-group'] }>
              <label className={ spectre['form-label'] } htmlFor="result_filter">
                Result filter
                <input className={ spectre['form-input'] } id="result_filter" size="25" type="text" />
              </label>
            </div>
          </div>
        </div>
      );
    }
    if (activetab === 7) {
      const { resultData } = this.props;
      const { expData } = this.props;
      return (
        <div>
          <ScatterChart data={ resultData } xtitle="I, A" ytitle="U, V" />
          <ScatterChart data={ expData } xtitle="I, A" ytitle="U, V" />
        </div>
      );
    }
    return null;
  }

  render() {
    const { activetab } = this.state;
    const { experiments } = this.props;
    const tabItem = spectre['tab-item'];
    const tabItemActive = [spectre['tab-item'], spectre.active].join(' ');
    return (
      <div className={ [styled.container, spectre['c-graphs']].join(' ') }>
        <ul className={ [spectre.tab, spectre['tab-block']].join(' ') } role="menu">
          <div className={ activetab === 6 ? `${ tabItemActive }` : `${ tabItem }` } onClick={() => this.tabChange(6)} role="menuitem" onKeyPress={() => {}} tabIndex={-1}>
            <span>Save / Load</span>
          </div>
          <div className={ activetab === 7 ? `${ tabItemActive }` : `${ tabItem }`} onClick={() => this.tabChange(7)} role="menuitem" onKeyPress={() => {}} tabIndex={-1}>
            <span>View Chart</span>
          </div>
        </ul>
        <div>
          { this.renderTab(experiments) }
        </div>
      </div>
    );
  }
}

const expShape = {
  name: PropTypes.string,
  data: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number))
};

Graphs.propTypes = {
  experiments: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.string)).isRequired,
  expData: PropTypes.arrayOf(PropTypes.shape(expShape)).isRequired,
  resultData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)).isRequired
};

const mapStateToProps = function mapStateToProps(store) {
  return {
    experiments: store.expState.experiments,
    expData: store.expState.expData,
    resultData: store.expState.resultData
  };
};

export default connect(mapStateToProps)(Graphs);
