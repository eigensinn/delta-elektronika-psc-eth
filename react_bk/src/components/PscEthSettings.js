import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import { savePwd, setRange, getPwdStatus, getRanges } from '../api/pscethSettings';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

class PscEthSettings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPwd: "",
      newPwd: "",
      pwdStatusValue: props.pwdStatus,
      rngOutputVoltage: props.range.rng_output_voltage,
      rngOutputCurrent: props.range.rng_output_current,
      rngMaxVoltage: props.range.rng_max_voltage,
      rngMaxCurrent: props.range.rng_max_current,
      rngGainSourceVoltage: props.range.rng_gain_source_voltage,
      rngGainSourceCurrent: props.range.rng_gain_source_current,
      rngOffsetSourceVoltage: props.range.rng_offset_source_voltage,
      rngOffsetSourceCurrent: props.range.rng_offset_source_current,
      rngGainMeasureVoltage: props.range.rng_gain_measure_voltage,
      rngGainMeasureCurrent: props.range.rng_gain_measure_current,
      rngOffsetMeasureVoltage: props.range.rng_offset_measure_voltage,
      rngOffsetMeasureCurrent: props.range.rng_offset_measure_current
    };
  }
  
  componentWillMount() {
    getRanges();
  };
  
  componentWillReceiveProps (nextProps) {
    const { range, pwdStatus } = nextProps;
    this.setState({
      pwdStatusValue: pwdStatus,
      rngOutputVoltage: range.rng_output_voltage,
      rngOutputCurrent: range.rng_output_current,
      rngMaxVoltage: range.rng_max_voltage,
      rngMaxCurrent: range.rng_max_current,
      rngGainSourceVoltage: range.rng_gain_source_voltage,
      rngGainSourceCurrent: range.rng_gain_source_current,
      rngOffsetSourceVoltage: range.rng_offset_source_voltage,
      rngOffsetSourceCurrent: range.rng_offset_source_current,
      rngGainMeasureVoltage: range.rng_gain_measure_voltage,
      rngGainMeasureCurrent: range.rng_gain_measure_current,
      rngOffsetMeasureVoltage: range.rng_offset_measure_voltage,
      rngOffsetMeasureCurrent: range.rng_offset_measure_current
    });
  }
  
  handleChange = event => {
    this.setState({ [event.target.id]: event.target.value });
  }
  
  handleOldPwd = event => {
    this.setState({ oldPwd: event.target.value });
  }
  
  handleNewPwd = event => {
    this.setState({ newPwd: event.target.value });
  }
  
  savePwd = () => {
    const { oldPwd, newPwd } = this.state;
    savePwd({ 'old_pwd': oldPwd, 'new_pwd': newPwd });
  }
  
  getPwdStatus = () => {
    getPwdStatus();
  }
  
  setRange = event => {
    setRange({ [event.target.name]: event.target.value, 'type': event.target.name });
  }

  render() {
    const { classes } = this.props;
    const {
      oldPwd,
      newPwd,
      pwdStatusValue,
      rngOutputVoltage,
      rngOutputCurrent,
      rngMaxVoltage,
      rngMaxCurrent,
      rngGainSourceVoltage,
      rngGainSourceCurrent,
      rngOffsetSourceVoltage,
      rngOffsetSourceCurrent,
      rngGainMeasureVoltage,
      rngGainMeasureCurrent,
      rngOffsetMeasureVoltage,
      rngOffsetMeasureCurrent
    } = this.state;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid container spacing={ 8 }>
            <Grid item xs={ 8 } sm={ 8 } md={ 8 } lg={ 8 }>
              <Card className={classes.card}>
                <CardContent>
                  <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Sliders Ranges
                  </Typography>
                  <TextField
                    id="rngOutputVoltage"
                    name="rng_output_voltage"
                    helperText="Output Voltage"
                    className={ classes.textField }
                    margin="normal"
                    value={ rngOutputVoltage }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                  />
                  <TextField
                    id="rngOutputCurrent"
                    name="rng_output_current"
                    helperText="Output Current"
                    className={classes.textField}
                    margin="normal"
                    value={ rngOutputCurrent }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                  />
                  <TextField
                    id="rngMaxVoltage"
                    name="rng_max_voltage"
                    helperText="Max Voltage"
                    className={classes.textField}
                    margin="normal"
                    value={ rngMaxVoltage }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                  />
                  <TextField
                    id="rngMaxCurrent"
                    name="rng_max_current"
                    helperText="Max Current"
                    className={classes.textField}
                    margin="normal"
                    value={ rngMaxCurrent }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                  />

                  <TextField
                    id="rngGainSourceVoltage"
                    name="rng_gain_source_voltage"
                    helperText="Gain Source Voltage?"
                    className={classes.textField}
                    margin="normal"
                    value={ rngGainSourceVoltage }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                  />
                  <TextField
                    id="rngGainSourceCurrent"
                    name="rng_gain_source_current"
                    helperText="Gain Source Current?"
                    className={classes.textField}
                    margin="normal"
                    value={ rngGainSourceCurrent }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                  />
                  <TextField
                    id="rngOffsetSourceVoltage"
                    name="rng_offset_source_voltage"
                    helperText="Offset Source Voltage?"
                    className={classes.textField}
                    margin="normal"
                    value={ rngOffsetSourceVoltage }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                  />
                  <TextField
                    id="rngOffsetSourceCurrent"
                    name="rng_offset_source_current"
                    helperText="Offset Source Current?"
                    className={classes.textField}
                    margin="normal"
                    value={ rngOffsetSourceCurrent }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                  />
                  
                  <TextField
                    id="rngGainMeasureVoltage"
                    name="rng_gain_measure_voltage"
                    helperText="Gain Measure Voltage?"
                    className={classes.textField}
                    margin="normal"
                    value={ rngGainMeasureVoltage }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                  />
                  <TextField
                    id="rngGainMeasureCurrent"
                    name="rng_gain_measure_current"
                    helperText="Gain Measure Current?"
                    className={classes.textField}
                    margin="normal"
                    value={ rngGainMeasureCurrent }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                  />
                  <TextField
                    id="rngOffsetMeasureVoltage"
                    name="rng_offset_measure_voltage"
                    helperText="Offset Measure Voltage?"
                    className={classes.textField}
                    margin="normal"
                    value={ rngOffsetMeasureVoltage }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                  />
                  <TextField
                    id="rngOffsetMeasureCurrent"
                    name="rng_offset_measure_current"
                    helperText="Offset Measure Current?"
                    className={classes.textField}
                    margin="normal"
                    value={ rngOffsetMeasureCurrent }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                  />
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs={ 4 } sm={ 4 } md={ 4 } lg={ 4 }>
              <Card className={classes.card}>
                <CardContent>
                  <TextField
                    id="old_pwd"
                    label="Old Password"
                    className={ classes.textField }
                    value={ oldPwd }
                    onChange={ this.handleOldPwd }
                    margin="normal"
                  />
                  <TextField
                    id="new_pwd"
                    label="New Password"
                    className={ classes.textField }
                    value={ newPwd }
                    onChange={ this.handleNewPwd }
                    margin="normal"
                  />
                </CardContent>
                <CardActions>
                  <Button
                    size="small"
                    variant="outlined"
                    onClick={ this.savePwd }
                  >Save Password</Button>
                  <Tooltip title={ pwdStatusValue } placement="top-end">
                    <Button
                      size="small"
                      variant="outlined"
                      onClick={ this.getPwdStatus }
                    >Password Set?</Button>
                  </Tooltip>
                </CardActions>
              </Card>
            </Grid>
          </Grid>
          <Grid container spacing={ 8 }>
            <Grid item xs={ 5 } sm={ 5 } md={ 5 } lg={ 5 }>
              <Card className={classes.card}>
                <CardContent>
                  <TextField
                    id="save_parameters"
                    label="Parameters name"
                    className={ classes.textField }
                    onChange={() => {}}
                    margin="normal"
                  />
                  <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="load_parameters">Load</InputLabel>
                    <Select
                      value=""
                      onChange={() => {}}
                      inputProps={{
                        name: 'load_parameters',
                        id: 'load_parameters',
                      }}
                    >
                      <MenuItem value="">
                        <em>None</em>
                      </MenuItem>
                      <MenuItem value={10}>Ten</MenuItem>
                      <MenuItem value={20}>Twenty</MenuItem>
                      <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                  </FormControl>
                </CardContent>
                <CardActions>
                  <Button size="small" variant="outlined">Save Parameters?</Button>
                  <Button size="small" variant="outlined">Delete Parameters</Button>
                </CardActions>
              </Card>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const rangeShape = {
  rng_output_voltage: PropTypes.number,
  rng_output_current: PropTypes.number,
  rng_max_voltage: PropTypes.number,
  rng_max_current: PropTypes.number,
  rng_gain_source_voltage: PropTypes.number,
  rng_gain_source_current: PropTypes.number,
  rng_offset_source_voltage: PropTypes.number,
  rng_offset_source_current: PropTypes.number,
  rng_gain_measure_voltage: PropTypes.number,
  rng_gain_measure_current: PropTypes.number,
  rng_offset_measure_voltage: PropTypes.number,
  rng_offset_measure_current: PropTypes.number
};

PscEthSettings.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  pwdStatus: PropTypes.number.isRequired,
  range: PropTypes.shape(rangeShape).isRequired
};

const mapStateToProps = function mapStateToProps(store) {
  return {
    pwdStatus: store.pscEthSettingsState.pwdStatus,
    range: store.pscEthSettingsState.range
  };
};

export default connect(mapStateToProps)(withStyles(styles, { withTheme: true })(PscEthSettings));