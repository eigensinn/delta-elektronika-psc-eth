import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Controls from './Controls';
import PscEth from './PscEth';

const App = () => (
  <Switch>
    <Route exact path='/' component={ Controls } />
    <Route path='/psceth' component={ PscEth } />
  </Switch>
);

export default App;
