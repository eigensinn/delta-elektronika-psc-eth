import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import { withSnackbar } from 'notistack';
import { getSelectedSequenceName, getSequencerData, triggerAStep, readSequenceMode, setSequenceMode } from '../api/pscethSequencer';
import { uploadSequence, createSequence, downloadSequence, getCatalog, delCatalog } from '../api/pscethCatalog';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

class PscEthSequenser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedSequence: props.seqData.selected_sequence,
      seqForUpload: props.seqData.seq_for_upload,
      seqModeValue: props.seqMode,
      uploadedSeqValue: props.uploadedSeq,
      downloadedSeqValue: props.downloadedSeq,
      seqNameValue: props.seqName,
      seqListValue: props.seqList
    };
  }
  
  componentWillMount() {
    getSequencerData();
  };
  
  componentWillReceiveProps (nextProps) {
    const { seqData, seqMode, uploadedSeq, downloadedSeq, seqName, seqList } = nextProps;
    this.setState({
      selectedSequence: seqData.selected_sequence,
      seqForUpload: seqData.seq_for_upload,
      seqModeValue: seqMode,
      uploadedSeqValue: uploadedSeq,
      downloadedSeqValue: downloadedSeq,
      seqNameValue: seqName,
      seqListValue: seqList
    });
  }

  getSelectedSequenceName = () => {
    // getCatalog();
    getSelectedSequenceName();
  }
  
  handleChange = event => {
    this.setState({ [event.target.id]: event.target.value });
  }
  
  triggerAStep = () => {
    triggerAStep();
    const { enqueueSnackbar } = this.props;
    enqueueSnackbar('Successfully triggered a step.', { variant: 'success' });
  }
  
  readSequenceMode = () => {
    readSequenceMode();
    const { enqueueSnackbar } = this.props;
    const { seqModeValue } = this.state;
    enqueueSnackbar(seqModeValue, { variant: 'info' });
  }
  
  setSequenceMode = event => {
    setSequenceMode({ 'type': event.target.id });
    const { enqueueSnackbar } = this.props;
    enqueueSnackbar(`Successfully set ${event.target.id}`, { variant: 'success' });
  }
  
  createSequence = () => {
    const { seqNameValue } = this.state;
    createSequence(seqNameValue);
  }
  
  uploadSequence = () => {
    const { selectedSequence, seqForUpload } = this.state;
    uploadSequence({ 'seq_name': selectedSequence, 'step_body': seqForUpload });
  }
  
  downloadSequence = () => {
    downloadSequence();
  }
  
  delCatalog = () => {
    delCatalog();
    const { enqueueSnackbar } = this.props;
    enqueueSnackbar('Successfully delete catalog.', { variant: 'success' });
  }
  
  readCatalog = () => {
    getCatalog();
    const { enqueueSnackbar } = this.props;
    enqueueSnackbar('Successfully fetched the data.', { variant: 'success' });
  }
  
  renderSelect () {
    const { selectedSequence, seqListValue } = this.state;
    return (
      <Select
        value={ selectedSequence }
        onChange={ this.createSequence }
        inputProps={{
          name: 'seq',
          id: 'seq',
        }}
      >
        { seqListValue.map((seq) => {
          return <MenuItem key={ seq } value={ seq }>{ seq }</MenuItem>;
        })
        }
      </Select>
    );
  };
  
  render() {
    const { classes } = this.props;
    const {
      seqForUpload,
      seqModeValue,
      seqNameValue,
      uploadedSeqValue,
      downloadedSeqValue,
    } = this.state;
    
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid container spacing={ 8 }>
            <Grid item xs={ 4 } sm={ 4 } md={ 4 } lg={ 4 }>
              <Card className={classes.card}>
                <CardContent>
                  <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Catalog Actions
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button
                    size="small"
                    variant="outlined"
                    onClick={ this.readCatalog }
                  >Read Catalog?</Button>
                  <Button
                    size="small"
                    variant="outlined"
                    onClick={ this.delCatalog }
                  >Delete Catalog</Button>
                </CardActions>
              </Card>
            </Grid>
            <Grid item xs={ 5 } sm={ 5 } md={ 5 } lg={ 5 }>
              <Card className={classes.card}>
                <CardContent>
                  <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="seq">Sequence</InputLabel>
                    { this.renderSelect() }
                  </FormControl>
                </CardContent>
                <CardActions>
                  <Button
                    size="small"
                    variant="outlined"
                    onClick={ this.getSelectedSequenceName }
                  >Selected Sequence?</Button>
                  <Button size="small" variant="outlined">Delete a Sequence</Button>
                </CardActions>
              </Card>
            </Grid>
            <Grid item xs={ 3 } sm={ 3 } md={ 3 } lg={ 3 }>
              <Card className={classes.card}>
                <CardContent>
                  <TextField
                    id="seqName"
                    label="New Seq."
                    className={ classes.textField }
                    onChange={ this.handleChange }
                    value={ seqNameValue }
                    margin="normal"
                  />
                </CardContent>
                <CardActions>
                  <Button
                    size="small"
                    variant="outlined"
                    onClick={ this.createSequence }
                  >Create / Select Seq.</Button>
                </CardActions>
              </Card>
            </Grid>
          </Grid>
          <Grid container spacing={ 8 }>
            <Grid item xs={ 6 } sm={ 6 } md={ 6 } lg={ 6 }>
              <Card className={classes.card}>
                <CardContent>
                  <Tooltip title="example: 1 sv=0" placement="top-start">
                    <TextField
                      id="seqForUpload"
                      label="Seq. for Upload (Step)"
                      multiline
                      rows="4"
                      title={ uploadedSeqValue }
                      className={ classes.textField }
                      value={ seqForUpload }
                      onChange={ this.handleChange }
                      margin="normal"
                    />
                  </Tooltip>
                  <TextField
                    id="selectedSequence"
                    label="Downloaded Seq. (Step)"
                    multiline
                    rows="4"
                    className={ classes.textField }
                    value={ downloadedSeqValue }
                    onChange={ this.handleChange }
                    margin="normal"
                  />
                </CardContent>
                <CardActions>
                  <Button 
                    size="small"
                    variant="outlined"
                    onClick={ this.uploadSequence }
                  >Upload Sequence</Button>
                  <Button
                    size="small"
                    variant="outlined"
                    onClick={ this.downloadSequence }
                  >Download Sequence</Button>
                </CardActions>
              </Card>
            </Grid>
            <Grid item xs={ 6 } sm={ 6 } md={ 6 } lg={ 6 }>
              <Card className={classes.card}>
                <CardActions>
                  <Button
                    id="RUN"
                    size="small"
                    title={ seqModeValue } // For delete
                    variant="outlined"
                    onClick={ this.setSequenceMode }
                  >Run</Button>
                  <Button
                    id="PAUSe"
                    size="small"
                    variant="outlined"
                    onClick={ this.setSequenceMode }
                  >Pause</Button>
                  <Button
                    id="CONTinue"
                    size="small"
                    variant="outlined"
                    onClick={ this.setSequenceMode }
                  >Continue</Button>
                  <Button
                    id="NEXT"
                    size="small"
                    variant="outlined"
                    onClick={ this.setSequenceMode }
                  >Next</Button>
                  <Button
                    id="STOP"
                    size="small"
                    variant="outlined"
                    onClick={ this.setSequenceMode }
                  >Stop</Button>
                  <Button
                    size="small"
                    variant="outlined"
                    onClick={ this.triggerAStep }
                  >Trigger a Step</Button>
                  <Tooltip title="Read Sequence Mode" placement="top-end">
                    <Button
                      size="small"
                      variant="outlined"
                      onClick={ this.readSequenceMode }
                    >State?</Button>
                  </Tooltip>
                </CardActions>
              </Card>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const sequenseShape = {
  selected_sequence: PropTypes.string.isRequired,
  seq_for_upload: PropTypes.string.isRequired
};

PscEthSequenser.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  enqueueSnackbar: PropTypes.func.isRequired,
  seqData: PropTypes.shape(sequenseShape).isRequired,
  seqMode: PropTypes.string.isRequired,
  uploadedSeq: PropTypes.string.isRequired,
  downloadedSeq: PropTypes.string.isRequired,
  seqName: PropTypes.string.isRequired,
  seqList: PropTypes.arrayOf(PropTypes.string).isRequired
};

const mapStateToProps = function mapStateToProps(store) {
  return {
    seqData: store.pscEthSequencerState.seqData,
    seqMode: store.pscEthSequencerState.seqMode,
    uploadedSeq: store.pscEthCatalogState.uploadedSeq,
    downloadedSeq: store.pscEthCatalogState.downloadedSeq,
    seqName: store.pscEthCatalogState.seqName,
    seqList: store.pscEthCatalogState.seqList
  };
};

export default connect(mapStateToProps)(withStyles(styles, { withTheme: true })(withSnackbar(PscEthSequenser)));