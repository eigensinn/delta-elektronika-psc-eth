import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import Tooltip from '@material-ui/core/Tooltip';
import { getRegister, getEventStatusRegister, getStatusByte, setRegister } from '../api/pscethRegister';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  card: {
    minWidth: 275,
  },
  title: {
    fontSize: 14,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4,
  },
});

class PscEthRegister extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openOp: false,
      openQu: false,
      eventStatusRegisterValue: props.eventStatusRegister,
      statusByteValue: props.statusByte,
      eventStatusEnable: props.registerData.event_status_enable,
      serviceRequestEnable: props.registerData.service_request_enable,
      positiveTransitionRegister: props.registerData.positive_transition_register,
      negativeTransitionRegister: props.registerData.negative_transition_register,
      enableState: props.registerData.enable_state,
      selectedIndex: 0,
      activeRegister: "user_input_registers"
    };
  }
  
  componentWillReceiveProps (nextProps) {
    const { eventStatusRegister, statusByte, registerData } = nextProps;
    this.setState({
      eventStatusRegisterValue: eventStatusRegister,
      statusByteValue: statusByte,
      eventStatusEnable: registerData.event_status_enable,
      serviceRequestEnable: registerData.service_request_enable,
      positiveTransitionRegister: registerData.positive_transition_register,
      negativeTransitionRegister: registerData.negative_transition_register,
      enableState: registerData.enable_state
    });
  }

  handleClickOp = () => {
    this.setState(state => ({ openOp: !state.openOp }));
  };
  
  handleClickQu = () => {
    this.setState(state => ({ openQu: !state.openQu }));
  };
  
  handleListItemClick = (event, index) => {
    this.setState({
      selectedIndex: index,
      activeRegister: event.target.id
    });
  }
  
  handleChange = event => {
    this.setState({ [event.target.id]: event.target.value });
  }
  
  getEventStatusRegister = () => {
    getEventStatusRegister();
  }
  
  getStatusByte = () => {
    getStatusByte();
  }
  
  setRegister = event => {
    const { activeRegister } = this.state;
    setRegister({ 'type': event.target.name, 'register': activeRegister });
  }
  
  getRegister = event => {
    const { activeRegister } = this.state;
    getRegister({ 'type': event.target.id, 'register': activeRegister });
  };

  render() {
    const { classes } = this.props;
    const {
      openOp,
      openQu,
      eventStatusRegisterValue,
      statusByteValue,
      eventStatusEnable,
      serviceRequestEnable,
      positiveTransitionRegister,
      negativeTransitionRegister,
      enableState,
      selectedIndex
    } = this.state;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid container spacing={ 8 }>
            <Grid item xs={ 6 } sm={ 6 } md={ 6 } lg={ 6 }>
              <Card className={classes.card}>
                <CardContent>
                  <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Actions
                  </Typography>
                  <TextField
                    id="eventStatusEnable"
                    name="event_status_enable"
                    label="ESE"
                    className={ classes.textField }
                    value={ eventStatusEnable }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                    margin="normal"
                  />
                  <TextField
                    id="serviceRequestEnable"
                    name="service_reques_enable"
                    label="SRE"
                    className={ classes.textField }
                    value={ serviceRequestEnable }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                    margin="normal"
                  />
                  <TextField
                    id="eventStatusRegisterValue"
                    label="ESR"
                    className={ classes.textField }
                    onChange={ this.handleChange }
                    margin="normal"
                    value={ eventStatusRegisterValue }
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                  <TextField
                    id="statusByteValue"
                    label="STB"
                    className={ classes.textField }
                    onChange={ this.handleChange }
                    margin="normal"
                    value={ statusByteValue }
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </CardContent>
                <CardActions>
                  <Tooltip title="Event Status Enable?" placement="top-start">
                    <Button
                      id="event_status_enable"
                      size="small"
                      variant="outlined"
                      onClick={ this.getRegister }
                    >ESE?</Button>
                  </Tooltip>
                  <Tooltip title="Service Request Enable?" placement="top">
                    <Button
                      id="service_request_enable"
                      size="small"
                      variant="outlined"
                      onClick={ this.getRegister }
                    >SRE?</Button>
                  </Tooltip>
                  <Tooltip title="Event Status Register?" placement="top">
                    <Button
                      id="event_status_register"
                      size="small"
                      variant="outlined"
                      onClick={ this.getEventStatusRegister }
                    >ESR?</Button>
                  </Tooltip>
                  <Tooltip title="Status Byte?" placement="top">
                    <Button
                      id="status_byte"
                      size="small"
                      variant="outlined"
                      onClick={ this.getStatusByte }
                    >STB?</Button>
                  </Tooltip>
                  <Tooltip title="Clear All Event Registers" placement="top-end">
                    <Button
                      id="clear_all_event_registers"
                      size="small"
                      variant="outlined"
                      onClick={ this.getRegister }
                    >CLS</Button>
                  </Tooltip>
                </CardActions>
              </Card>
            </Grid>
            <Grid item xs={ 6 } sm={ 6 } md={ 6 } lg={ 6 }>
              <Card className={classes.card}>
                <CardContent>
                  <List
                    component="nav"
                    subheader={<ListSubheader component="div">Register Type Choice</ListSubheader>}
                    className={classes.root}
                  >
                    <ListItem
                      button
                      id="user_input_registers"
                      selected={ selectedIndex === 0 }
                      onClick={ (event) => this.handleListItemClick(event, 0) }
                    >
                      <ListItemIcon>
                        <InboxIcon />
                      </ListItemIcon>
                      <ListItemText inset primary="User Input Registers" />
                    </ListItem>
                    <ListItem button onClick={this.handleClickOp}>
                      <ListItemIcon>
                        <InboxIcon />
                      </ListItemIcon>
                      <ListItemText inset primary="Status Operation Register Set" />
                      { openOp ? <ExpandLess /> : <ExpandMore /> }
                    </ListItem>
                    <Collapse in={ openOp } timeout="auto" unmountOnExit>
                      <List component="div" disablePadding>
                        <ListItem
                          button
                          id="status_operation_registers"
                          selected={ selectedIndex === 1 }
                          onClick={ (event) => this.handleListItemClick(event, 1) }
                          className={classes.nested}
                        >
                          <ListItemIcon>
                            <StarBorder />
                          </ListItemIcon>
                          <ListItemText inset primary="Status Operation Registers" />
                        </ListItem>
                        <ListItem
                          button
                          id="status_operation_settings_registers"
                          selected={ selectedIndex === 2 }
                          onClick={ (event) => this.handleListItemClick(event, 2) }
                          className={classes.nested}
                        >
                          <ListItemIcon>
                            <StarBorder />
                          </ListItemIcon>
                          <ListItemText inset primary="Status Operation Settings Registers" />
                        </ListItem>
                        <ListItem
                          button
                          id="status_operation_regulating_registers"
                          className={classes.nested}
                          selected={ selectedIndex === 3 }
                          onClick={ (event) => this.handleListItemClick(event, 3) }
                        >
                          <ListItemIcon>
                            <StarBorder />
                          </ListItemIcon>
                          <ListItemText inset primary="Status Operation Regulating Registers" />
                        </ListItem>
                        <ListItem
                          button
                          id="status_operation_rcontrol_registers"
                          className={classes.nested}
                          selected={ selectedIndex === 4 }
                          onClick={ (event) => this.handleListItemClick(event, 4) }
                        >
                          <ListItemIcon>
                            <StarBorder />
                          </ListItemIcon>
                          <ListItemText inset primary="Status Operation RControl Registers" />
                        </ListItem>
                        <ListItem
                          button
                          id="status_operation_shutdown_registers"
                          className={classes.nested}
                          selected={ selectedIndex === 5 }
                          onClick={ (event) => this.handleListItemClick(event, 5) }
                        >
                          <ListItemIcon>
                            <StarBorder />
                          </ListItemIcon>
                          <ListItemText inset primary="Status Operation Shutdown Registers" />
                        </ListItem>
                        <ListItem
                          button
                          id="status_operation_shutdown_protection_registers"
                          selected={ selectedIndex === 6 }
                          onClick={ (event) => this.handleListItemClick(event, 6) }
                          className={classes.nested}
                        >
                          <ListItemIcon>
                            <StarBorder />
                          </ListItemIcon>
                          <ListItemText inset primary="Status Operation Shutdown Protection Registers" />
                        </ListItem>
                      </List>
                    </Collapse>
                    <ListItem button onClick={this.handleClickQu}>
                      <ListItemIcon>
                        <InboxIcon />
                      </ListItemIcon>
                      <ListItemText inset primary="Status Questionable Register Set" />
                      { openQu ? <ExpandLess /> : <ExpandMore /> }
                    </ListItem>
                    <Collapse in={ openQu } timeout="auto" unmountOnExit>
                      <List component="div" disablePadding>
                        <ListItem
                          button
                          id="status_questionable_registers"
                          selected={ selectedIndex === 7 }
                          onClick={ (event) => this.handleListItemClick(event, 7) }
                          className={classes.nested}
                        >
                          <ListItemIcon>
                            <DraftsIcon />
                          </ListItemIcon>
                          <ListItemText inset primary="Status Questionable Registers" />
                        </ListItem>
                        <ListItem
                          button
                          id="status_questionable_voltage_registers"
                          selected={ selectedIndex === 8 }
                          onClick={ (event) => this.handleListItemClick(event, 8) }
                          className={classes.nested}
                        >
                          <ListItemIcon>
                            <SendIcon />
                          </ListItemIcon>
                          <ListItemText inset primary="Status Questionable Voltage Registers" />
                        </ListItem>
                        <ListItem
                          button
                          id="status_questionable_current_registers"
                          className={classes.nested}
                          selected={ selectedIndex === 9 }
                          onClick={ (event) => this.handleListItemClick(event, 9) }
                        >
                          <ListItemIcon>
                            <SendIcon />
                          </ListItemIcon>
                          <ListItemText inset primary="Status Questionable Current Registers" />
                        </ListItem>
                      </List>
                    </Collapse>
                  </List>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
          <Grid container spacing={ 8 }>
            <Grid item xs={ 6 } sm={ 6 } md={ 6 } lg={ 6 }>
              <Card className={classes.card}>
                <CardContent>
                  <TextField
                    id="positiveTransitionRegister"
                    name="positive_transition_register"
                    label="PTR"
                    className={ classes.textField }
                    value={ positiveTransitionRegister }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                    margin="normal"
                  />
                  <TextField
                    id="negativeTransitionRegister"
                    name="negative_transition_register"
                    label="NTR"
                    className={ classes.textField }
                    value={ negativeTransitionRegister }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                    margin="normal"
                  />
                  <TextField
                    id="enableState"
                    name="enable_state"
                    label="Enable Register"
                    className={ classes.textField }
                    value={ enableState }
                    onChange={ this.handleChange }
                    onDoubleClick={ this.setRange }
                    margin="normal"
                  />
                </CardContent>
                <CardActions>
                  <Button
                    id="condition"
                    size="small"
                    variant="outlined"
                    onClick={ this.getRegister }
                  >Condition?</Button>
                  <Tooltip title="Positive Transition Register?" placement="top">
                    <Button
                      id="positive_transition_register"
                      size="small"
                      variant="outlined"
                      onClick={ this.getRegister }
                    >PTR?</Button>
                  </Tooltip>
                  <Tooltip title="Negative Transition Register?" placement="top">
                    <Button
                      id="negative_transition_register"
                      size="small"
                      variant="outlined"
                      onClick={ this.getRegister }
                    >NTR?</Button>
                  </Tooltip>
                  <Button
                    id="event_state"
                    size="small"
                    variant="outlined"
                    onClick={ this.getRegister }
                  >Event?</Button>
                  <Button
                    id="enable_state"
                    size="small"
                    variant="outlined"
                    onClick={ this.getRegister }
                  >Enable Register?</Button>
                </CardActions>
              </Card>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const registerShape = {
  event_status_enable: PropTypes.number,
  service_request_enable: PropTypes.number,
  positive_transition_register: PropTypes.number,
  negative_transition_register: PropTypes.number,
  enable_state: PropTypes.number
};

PscEthRegister.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  eventStatusRegister: PropTypes.number.isRequired,
  statusByte: PropTypes.number.isRequired,
  registerData: PropTypes.shape(registerShape).isRequired
};

const mapStateToProps = function mapStateToProps(store) {
  return {
    eventStatusRegister: store.pscEthRegisterState.eventStatusRegister,
    statusByte: store.pscEthRegisterState.statusByte,
    registerData: store.pscEthRegisterState.registerData
  };
};

export default connect(mapStateToProps)(withStyles(styles, { withTheme: true })(PscEthRegister));