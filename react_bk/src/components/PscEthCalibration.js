import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import VerticalSlider from './VerticalSlider';
import { getCalibration, calibrate } from '../api/pscethCalibration';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  }
});

class PscEthCalibration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gainSourceVoltage: props.calibrationData.gain_source_voltage,
      gainSourceCurrent: props.calibrationData.gain_source_current,
      offsetSourceVoltage: props.calibrationData.offset_source_voltage,
      offsetSourceCurrent: props.calibrationData.offset_source_current,
      gainMeasureVoltage: props.calibrationData.gain_measure_voltage,
      gainMeasureCurrent: props.calibrationData.gain_measure_current,
      offsetMeasureVoltage: props.calibrationData.offset_measure_voltage,
      offsetMeasureCurrent: props.calibrationData.offset_measure_current
    };
  }
  
  componentWillMount() {
    getCalibration();
  };
  
  componentWillReceiveProps (nextProps) {
    const { calibrationData } = nextProps;
    this.setState({
      gainSourceVoltage: calibrationData.gain_source_voltage,
      gainSourceCurrent: calibrationData.gain_source_current,
      offsetSourceVoltage: calibrationData.offset_source_voltage,
      offsetSourceCurrent: calibrationData.offset_source_current,
      gainMeasureVoltage: calibrationData.gain_measure_voltage,
      gainMeasureCurrent: calibrationData.gain_measure_current,
      offsetMeasureVoltage: calibrationData.offset_measure_voltage,
      offsetMeasureCurrent: calibrationData.offset_measure_current
    });
  }
  
  calibrateSld = event => {
    const type = event.target.id;
    this.setState({ [type]: event.target.value });
    calibrate({ [event.target.name]: event.target.value, 'type': type });
  }
  
  calibrate = event => {
    const type = event.target.name.replace('inp_', '');
    calibrate({ [type]: event.target.value, 'type': type });
  }
  
  handleChange = event => {
    this.setState({ [event.target.id.replace("Inp", "")]: event.target.value });
  }
  
  render() {
    const { classes } = this.props;
    const {
      gainSourceVoltage,
      gainSourceCurrent,
      offsetSourceVoltage,
      offsetSourceCurrent,
      gainMeasureVoltage,
      gainMeasureCurrent,
      offsetMeasureVoltage,
      offsetMeasureCurrent
    } = this.state;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid container spacing={ 8 }>
            <Grid item xs={ 3 } sm={ 3 } md={ 3 } lg={ 3 }>
              <TextField
                id="gainSourceVoltageInp"
                name="inp_gain_source_voltage"
                label="Gain Source Voltage?"
                className={classes.textField}
                value={ gainSourceVoltage || 0 }
                onChange={ this.handleChange }
                onDoubleClick={ this.calibrate }
                margin="normal"
                variant="outlined"
              />
              <VerticalSlider
                id="gainSourceVoltage"
                name="gain_source_voltage"
                value={ gainSourceVoltage }
                onChange={ this.calibrateSld }
              />
            </Grid>
            <Grid item xs={ 3 } sm={ 3 } md={ 3 } lg={ 3 }>
              <TextField
                id="gainSourceCurrentInp"
                name="inp_gain_source_current"
                label="Gain Source Current?"
                className={ classes.textField }
                value={ gainSourceCurrent || 0 }
                onChange={ this.handleChange }
                onDoubleClick={ this.calibrate }
                margin="normal"
                variant="outlined"
              />
              <VerticalSlider
                id="gainSourceCurrent"
                name="gain_source_current"
                value={ gainSourceCurrent }
                onChange={ this.calibrateSld }
              />
            </Grid>
            <Grid item xs={ 3 } sm={ 3 } md={ 3 } lg={ 3 }>
              <TextField
                id="offsetSourceVoltageInp"
                name="inp_offset_source_voltage"
                label="Offset Source Voltage?"
                className={classes.textField}
                value={ offsetSourceVoltage || 0 }
                onChange={ this.handleChange }
                onDoubleClick={ this.calibrate }
                margin="normal"
                variant="outlined"
              />
              <VerticalSlider
                id="offsetSourceVoltage"
                name="offset_source_voltage"
                value={ offsetSourceVoltage }
                onChange={ this.calibrateSld }
              />
            </Grid>
            <Grid item xs={ 3 } sm={ 3 } md={ 3 } lg={ 3 }>
              <TextField
                id="offsetSourceCurrentInp"
                name="inp_offset_source_current"
                label="Offset Source Current?"
                className={ classes.textField }
                value={ offsetSourceCurrent || 0 }
                onChange={ this.handleChange }
                onDoubleClick={ this.calibrate }
                margin="normal"
                variant="outlined"
              />
              <VerticalSlider
                id="offsetSourceCurrent"
                name="offset_source_current"
                value={ offsetSourceCurrent }
                onChange={ this.calibrateSld }
              />
            </Grid>
          </Grid>
          <Grid container spacing={ 8 }>
            <Grid item xs={ 3 } sm={ 3 } md={ 3 } lg={ 3 }>
              <TextField
                id="gainMeasureVoltageInp"
                name="inp_gain_measure_voltage"
                label="Gain Measure Voltage?"
                className={classes.textField}
                value={ gainMeasureVoltage || 0 }
                onChange={ this.handleChange }
                onDoubleClick={ this.calibrate }
                margin="normal"
                variant="outlined"
              />
              <VerticalSlider
                id="gainMeasureVoltage"
                name="gain_measure_voltage"
                value={ gainMeasureVoltage || 0 }
                onChange={ this.calibrateSld }
              />
            </Grid>
            <Grid item xs={ 3 } sm={ 3 } md={ 3 } lg={ 3 }>
              <TextField
                id="gainMeasureCurrentInp"
                name="inp_gain_measure_current"
                label="Gain Measure Current?"
                className={ classes.textField }
                value={ gainMeasureCurrent || 0 }
                onChange={ this.handleChange }
                onDoubleClick={ this.calibrate }
                margin="normal"
                variant="outlined"
              />
              <VerticalSlider
                id="gainMeasureCurrent"
                name="gain_measure_current"
                value={ gainMeasureCurrent || 0 }
                onChange={ this.calibrateSld }
              />
            </Grid>
            <Grid item xs={ 3 } sm={ 3 } md={ 3 } lg={ 3 }>
              <TextField
                id="offsetMeasureVoltageInp"
                name="inp_offset_measure_voltage"
                label="Offset Measure Voltage?"
                className={classes.textField}
                value={ offsetMeasureVoltage || 0 }
                onChange={ this.handleChange }
                onDoubleClick={ this.calibrate }
                margin="normal"
                variant="outlined"
              />
              <VerticalSlider
                id="offsetMeasureVoltageInp"
                name="offset_measure_voltage"
                value={ offsetMeasureVoltage }
                onChange={ this.calibrateSld }
              />
            </Grid>
            <Grid item xs={ 3 } sm={ 3 } md={ 3 } lg={ 3 }>
              <TextField
                id="offsetMeasureCurrentInp"
                name="inp_offset_measure_current"
                label="Offset Measure Current?"
                className={ classes.textField }
                value={ offsetMeasureCurrent || 0 }
                onChange={ this.handleChange }
                onDoubleClick={ this.calibrate }
                margin="normal"
                variant="outlined"
              />
              <VerticalSlider
                id="offsetMeasureCurrent"
                name="offset_measure_current"
                value={ offsetMeasureCurrent || 0 }
                onChange={ this.calibrateSld }
              />
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const calibrationShape = {
  gain_source_voltage: PropTypes.number,
  gain_source_current: PropTypes.number,
  offset_source_voltage: PropTypes.number,
  offset_source_current: PropTypes.number,
  gain_measure_voltage: PropTypes.number,
  gain_measure_current: PropTypes.number,
  offset_measure_voltage: PropTypes.number,
  offset_measure_current: PropTypes.number,
};

PscEthCalibration.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  calibrationData: PropTypes.shape(calibrationShape).isRequired
};

const mapStateToProps = function mapStateToProps(store) {
  return {
    calibrationData: store.pscEthCalibrationState.calibrationData
  };
};

export default connect(mapStateToProps)(withStyles(styles, { withTheme: true })(PscEthCalibration));