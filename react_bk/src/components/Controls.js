import React from "react";
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from '../css/controls.css';
import spectre from '../css/spectre.scss';
// import spectre from '../../node_modules/spectre.css/dist/spectre.min.css';
import { getSavers, loadSaver, saveSaver } from '../api/savers';
import { getControls, setControl } from '../api/controls';
import Graphs from './Graphs';

class Controls extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activetab: 1,
      maxValue: 0,
      power: 0,
      portValue: props.controls.port,
      baudValue: props.controls.baudrate,
      modeValue: props.controls.mode,
      ccCurrentValue: props.controls.cc_current,
      cvVoltageValue: props.controls.cv_voltage,
      cwPowerValue: props.controls.cw_power,
      crResistanceValue: props.controls.cr_resistance,
      maxCurrentValue: props.controls.max_current,
      maxVoltageValue: props.controls.max_voltage,
      maxPowerValue: props.controls.max_power,
      powerDensityValue: props.controls.power_density,
      areaOfCellValue: props.controls.area_of_cell,
      minValue: props.controls.min_value,
      factorValue: props.controls.factor,
      quantityOfDotsValue: props.controls.quantity_of_dots,
      timeSleepValue: props.controls.time_sleep,
      onOffValue: props.controls.on_off,
      senseValue: props.controls.sense,
      locRemValue: props.controls.loc_rem,
      dimPowerValue: props.controls.dim_power,
      dimAreaValue: props.controls.dim_area,
      saversValue: props.savers
    };
  }
  
  componentWillMount() {
    getControls();
    getSavers();
  };
  
  componentWillReceiveProps (nextProps) {
    const { controls, savers } = nextProps;
    this.setState({
      portValue: controls.port,
      baudValue: controls.baudrate,
      modeValue: controls.mode,
      ccCurrentValue: controls.cc_current,
      cvVoltageValue: controls.cv_voltage,
      cwPowerValue: controls.cw_power,
      crResistanceValue: controls.cr_resistance,
      maxCurrentValue: controls.max_current,
      maxVoltageValue: controls.max_voltage,
      maxPowerValue: controls.max_power,
      powerDensityValue: controls.power_density,
      areaOfCellValue: controls.area_of_cell,
      minValue: controls.min_value,
      factorValue: controls.factor,
      quantityOfDotsValue: controls.quantity_of_dots,
      timeSleepValue: controls.time_sleep,
      onOffValue: controls.on_off,
      senseValue: controls.sense,
      locRemValue: controls.loc_rem,
      dimPowerValue: controls.dim_power,
      dimAreaValue: controls.dim_area,
      maxValue: controls.min_value + controls.factor * controls.quantity_of_dots,
      power: (controls.power_density * controls.area_of_cell).toFixed(2),
      saversValue: savers
    });
  }
  
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  }
    
  tabChange = (tabNumber) => {
    this.setState({
      activetab: tabNumber
    });
  };

  loadSaver = event => {
    loadSaver({ 'saver_name': event.target.value, 'preset': '{}' });
  }

  saveSaver = event => {
    saveSaver({ 'saver_name': event.target.value, 'preset': '{}' });
  }
  
  setControl = event => {
    const { controls } = this.props;
    const targetId = event.target.id.toString();
    const dict = { 'port': controls.port, 'baudrate': controls.baudrate, 'type': targetId };
    if (['b_4800', 'b_9600', 'b_19200', 'b_38400'].includes(targetId)) {
      dict.type = 'baudrate';
      dict.baudrate = event.target.value;
    } else if (['cc', 'cv', 'cw', 'cr'].includes(targetId)) {
      dict.type = 'mode';
      dict.mode = event.target.value;
    } else if ([
      'port', 'cc_current', 'cv_voltage', 'cw_power', 'cr_resistance',
      'max_current', 'max_voltage', 'max_power', 'power_density', 'area_of_cell', 'min_value',
      'factor', 'quantity_of_dots', 'time_sleep'].includes(targetId)) {
      dict.type = targetId;
      dict[dict.type] = event.target.value;
    } else if ([
      'on_off', 'sense', 'loc_rem'].includes(targetId)) {
      dict[dict.type] = event.target.checked;
    } else if (['power_density_unit_1', 'power_density_unit_2', 'power_density_unit_3'].includes(targetId)) {
      dict.type = 'dim_power';
      dict.dim_power = event.target.value;
      dict.dim_area = dict.dim_power.split('/').pop();
    } else if (['area_unit_1', 'area_unit_2'].includes(targetId)) {
      dict.type = 'dim_area';
      dict.dim_area = event.target.value;
      dict.dim_power = `W/${dict.dim_area}`;
    }
    setControl(dict);
  };
  
  renderSelect () {
    const { saversValue } = this.state;
    return (
      <select className={ spectre['form-select'] } id="saver_select">
        <option value="None">No selected</option>
        { saversValue.map((saver) => {
          return <option key={ saver } value={ saver } onClick={ this.loadSaver }>{ saver }</option>;
        })
        }
      </select>
    );
  };
  
  renderTab () {
    const { activetab } = this.state;
    const {
      portValue,
      baudValue,
      modeValue,
      ccCurrentValue,
      cvVoltageValue,
      cwPowerValue,
      crResistanceValue,
      maxCurrentValue,
      maxVoltageValue,
      maxPowerValue,
      powerDensityValue,
      areaOfCellValue,
      minValue,
      factorValue,
      quantityOfDotsValue,
      timeSleepValue,
      dimPowerValue,
      dimAreaValue
    } = this.state;
    if (activetab === 1) {
      return (
        <div>
          <div className={ spectre['form-group'] }>
            <label className={ spectre['form-label'] } htmlFor="port">
              Port
              <input
                className={ spectre['form-input'] }
                id="port"
                name="portValue"
                size="3"
                type="text"
                value={ portValue }
                onChange={ this.handleChange }
                onDoubleClick={ this.setControl }
              />
            </label>
          </div>
          <div className={ spectre['form-group'] }>
            Baudrate
            <label className={ spectre['form-label'] } htmlFor="baud">
              <label className={ [spectre['form-radio'], spectre['form-inline']].join(' ') } htmlFor="b_4800">
                <input
                  type="radio"
                  id="b_4800"
                  name="baudValue"
                  value="4800"
                  checked={ baudValue === 4800 }
                  onChange={ this.setControl }
                />
                <i className={ spectre['form-icon'] }></i> 4800
              </label>
              <label className={ [spectre['form-radio'], spectre['form-inline']].join(' ') } htmlFor="b_9600">
                <input
                  type="radio"
                  id="b_9600"
                  name="baudValue"
                  value="9600"
                  checked={ baudValue === 9600 }
                  onChange={ this.setControl }
                />
                <i className={ spectre['form-icon'] }></i> 9600
              </label>
              <label className={ [spectre['form-radio'], spectre['form-inline']].join(' ') } htmlFor="b_19200">
                <input
                  type="radio"
                  id="b_19200"
                  name="baudValue"
                  value="19200"
                  checked={ baudValue === 19200 }
                  onChange={ this.setControl }
                />
                <i className={ spectre['form-icon'] }></i> 19200
              </label>
              <label className={ [spectre['form-radio'], spectre['form-inline']].join(' ') } htmlFor="b_38400">
                <input
                  type="radio"
                  id="b_38400"
                  name="baudValue"
                  value="38400"
                  checked={ baudValue === 38400 }
                  onChange={ this.setControl }
                />
                <i className={ spectre['form-icon'] }></i> 38400
              </label>
            </label>
          </div>
          <div className={ spectre['form-group'] }>
            Mode
            <label className={ spectre['form-label'] } htmlFor="mode">
              <label className={ [spectre['form-radio'], spectre['form-inline']].join(' ') } htmlFor="cc">
                <input
                  type="radio"
                  id="cc"
                  name="modeValue"
                  value="cc"
                  checked={ modeValue === "cc" }
                  onChange={ this.setControl }
                />
                <i className={ spectre['form-icon'] }></i> cc
              </label>
              <label className={ [spectre['form-radio'], spectre['form-inline']].join(' ') } htmlFor="cv">
                <input
                  type="radio"
                  id="cv"
                  name="modeValue"
                  value="cv"
                  checked={ modeValue === "cv" }
                  onChange={ this.setControl }
                />
                <i className={ spectre['form-icon'] }></i> cv
              </label>
              <label className={ [spectre['form-radio'], spectre['form-inline']].join(' ') } htmlFor="cw">
                <input
                  type="radio"
                  id="cw"
                  name="modeValue"
                  value="cw"
                  checked={ modeValue === "cw" }
                  onChange={ this.setControl }
                />
                <i className={ spectre['form-icon'] }></i> cw
              </label>
              <label className={ [spectre['form-radio'], spectre['form-inline']].join(' ') } htmlFor="cr">
                <input
                  type="radio"
                  id="cr"
                  name="modeValue"
                  value="cr"
                  checked={ modeValue === "cr" }
                  onChange={ this.setControl }
                />
                <i className={ spectre['form-icon'] }></i> cr
              </label>
            </label>
          </div>
        </div>
      );
    }
    if (activetab === 2) {
      return (
        <div className={ spectre.columns }>
          <div className={ [spectre.column, spectre['col-6']].join(' ') }>
            <div className={ spectre['form-group'] }>
              <label className={ spectre['form-label'] } htmlFor="cc_current">
                Set CC Current
                <input
                  className={ spectre['form-input'] }
                  id="cc_current"
                  name="ccCurrentValue"
                  size="10"
                  type="text"
                  value={ ccCurrentValue }
                  onChange={ this.handleChange }
                  onDoubleClick={ this.setControl }
                />
              </label>
            </div>
            <div className={ spectre['form-group'] }>
              <label className={ spectre['form-label'] } htmlFor="cv_voltage">
                Set CV Voltage
                <input
                  className={ spectre['form-input'] }
                  id="cv_voltage"
                  name="cvVoltageValue"
                  size="10"
                  type="text"
                  value={ cvVoltageValue }
                  onChange={ this.handleChange }
                  onDoubleClick={ this.setControl }
                />
              </label>
            </div>
            <div className={ spectre['form-group'] }>
              <label className={ spectre['form-label'] } htmlFor="cw_power">
                Set CW Power
                <input
                  className={ spectre['form-input'] }
                  id="cw_power"
                  name="cwPowerValue"
                  size="10"
                  type="text"
                  value={ cwPowerValue }
                  onChange={ this.handleChange }
                  onDoubleClick={ this.setControl }
                />
              </label>
            </div>
            <div className={ spectre['form-group'] }>
              <label className={ spectre['form-label'] } htmlFor="cr_resistance">
                Set CR Resistance
                <input
                  className={ spectre['form-input'] }
                  id="cr_resistance"
                  name="crResistanceValue"
                  size="10"
                  type="text"
                  value={ crResistanceValue }
                  onChange={ this.handleChange }
                  onDoubleClick={ this.setControl }
                />
              </label>
            </div>
          </div>
          <div className={ [spectre.column, spectre['col-6']].join(' ') }>
            <div className={ spectre['form-group'] }>
              <label className={ spectre['form-label'] } htmlFor="max_current">
                Set Max Current
                <input
                  className={ spectre['form-input'] }
                  id="max_current"
                  name="maxCurrentValue"
                  size="10"
                  type="text"
                  value={ maxCurrentValue }
                  onChange={ this.handleChange }
                  onDoubleClick={ this.setControl }
                />
              </label>
            </div>
            <div className={ spectre['form-group'] }>
              <label className={ spectre['form-label'] } htmlFor="max_voltage">
                Set Max Voltage
                <input
                  className={ spectre['form-input'] }
                  id="max_voltage"
                  name="maxVoltageValue"
                  size="10"
                  type="text"
                  value={ maxVoltageValue }
                  onChange={ this.handleChange }
                  onDoubleClick={ this.setControl }
                />
              </label>
            </div>
            <div className={ spectre['form-group'] }>
              <label className={ spectre['form-label'] } htmlFor="max_power">
                Set Max Power
                <input
                  className={ spectre['form-input'] }
                  id="max_power"
                  name="maxPowerValue"
                  size="10"
                  type="text"
                  value={ maxPowerValue }
                  onChange={ this.handleChange }
                  onDoubleClick={ this.setControl }
                />
              </label>
            </div>
          </div>
        </div>
      );
    }
    if (activetab === 3) {
      const { maxValue, power } = this.state;
      const formGroupDInlineBlock = [spectre['form-group'], spectre['d-inline-block']].join(' ');
      const formRadioFormInline = [spectre['form-radio'], spectre['form-inline']].join(' ');
      return (
        <div>
          <div>
            <div className={ formGroupDInlineBlock }>
              <label className={ spectre['form-label'] } htmlFor="power_density">
                Power Density
                <input
                  className={ spectre['form-input'] }
                  id="power_density"
                  size="10"
                  type="text"
                  name="powerDensityValue"
                  value={ powerDensityValue }
                  onChange={ this.handleChange }
                  onDoubleClick={ this.setControl }
                />
              </label>
            </div>
            <div className={ formGroupDInlineBlock }>
              <label className={ formRadioFormInline } htmlFor="power_density_unit_1">
                <input
                  className="units"
                  type="radio"
                  id="power_density_unit_1"
                  name="dimPowerValue"
                  value="mW/cm2"
                  checked={ dimPowerValue === "mW/cm2" }
                  onChange={ this.setControl }
                />
                <i className={ spectre['form-icon'] }></i> mW/cm2
              </label>
              <label className={ formRadioFormInline } htmlFor="power_density_unit_2">
                <input
                  className="units"
                  type="radio"
                  id="power_density_unit_2"
                  name="dimPowerValue"
                  value="W/cm2"
                  checked={ dimPowerValue === "W/cm2" }
                  onChange={ this.setControl }
                />
                <i className={ spectre['form-icon'] }></i> W/cm2
              </label>
              <label className={ formRadioFormInline } htmlFor="power_density_unit_3">
                <input
                  className="units"
                  type="radio"
                  id="power_density_unit_3"
                  name="dimPowerValue"
                  value="W/m2"
                  checked={ dimPowerValue === "W/m2" }
                  onChange={ this.setControl }
                />
                <i className={ spectre['form-icon'] }></i> W/m2
              </label>
            </div>
            <div className={ formGroupDInlineBlock }> × </div>
            <div className={ formGroupDInlineBlock }>
              <label className={ spectre['form-label'] } htmlFor="area_of_cell">
                Area of Cell
                <input
                  className={ spectre['form-input'] }
                  id="area_of_cell"
                  size="10"
                  type="text"
                  name="areaOfCellValue"
                  value={ areaOfCellValue }
                  onChange={ this.handleChange }
                  onDoubleClick={ this.setControl }
                />
              </label>
            </div>
            <div className={ formGroupDInlineBlock }>
              <label className={ formRadioFormInline } htmlFor="area_unit_1">
                <input
                  className="area_units"
                  type="radio"
                  id="area_unit_1"
                  name="dimAreaValue"
                  value="cm2"
                  checked={ dimAreaValue === "cm2" }
                  onChange={ this.setControl }
                />
                <i className={ spectre['form-icon'] }></i> cm2
              </label>
              <label className={ formRadioFormInline } htmlFor="area_unit_2">
                <input
                  className="area_units"
                  type="radio"
                  id="area_unit_2"
                  name="dimAreaValue"
                  value="m2"
                  checked={ dimAreaValue === "m2" }
                  onChange={ this.setControl }
                />
                <i className={ spectre['form-icon'] }></i> m2
              </label>
            </div>
            <div className={ formGroupDInlineBlock }> = </div>
            <div className={ formGroupDInlineBlock }>
              <label className={ spectre['form-label'] } htmlFor="power">
                Power
                <input
                  className={ spectre['form-input'] }
                  id="power"
                  size="10"
                  type="text"
                  name="power"
                  readOnly="readonly"
                  value={ power }
                />
              </label>
            </div>
          </div>
          <div>
            <div className={ formGroupDInlineBlock }>
              <label className={ spectre['form-label'] } htmlFor="min_value">
                Min Value (V/A)
                <input
                  className={ spectre['form-input'] }
                  id="min_value"
                  size="10"
                  type="text"
                  name="minValue"
                  value={ minValue }
                  onChange={ this.handleChange }
                  onDoubleClick={ this.setControl }
                />
              </label>
            </div>
            <div className={ formGroupDInlineBlock }> + </div>
            <div className={ formGroupDInlineBlock }>
              <label className={ spectre['form-label'] } htmlFor="factor">
                U/I Factor
                <input
                  className={ spectre['form-input'] }
                  id="factor"
                  size="10"
                  type="text"
                  name="factorValue"
                  value={ factorValue }
                  onChange={ this.handleChange }
                  onDoubleClick={ this.setControl }
                />
              </label>
            </div>
            <div className={ formGroupDInlineBlock }> × </div>
            <div className={ formGroupDInlineBlock }>
              <label className={ spectre['form-label'] } htmlFor="quantity_of_dots">
                Quantity of Dots
                <input
                  className={ spectre['form-input'] }
                  id="quantity_of_dots"
                  size="10"
                  type="text"
                  name="quantityOfDotsValue"
                  value={ quantityOfDotsValue }
                  onChange={ this.handleChange }
                  onDoubleClick={ this.setControl }
                />
              </label>
            </div>
            <div className={ formGroupDInlineBlock }> = </div>
            <div className={ formGroupDInlineBlock }>
              <label className={ spectre['form-label'] } htmlFor="max_value">
                Max Value
                <input
                  className={ spectre['form-input'] }
                  id="max_value"
                  size="10"
                  type="text"
                  name="max_value"
                  readOnly="readonly"
                  value={ maxValue }
                />
              </label>
            </div>
          </div>
          <div>
            <div className={ formGroupDInlineBlock }>
              <label className={ spectre['form-label'] } htmlFor="time_sleep" id="lbl_time_sleep">
                Time Sleep
                <input
                  className={ spectre['form-input'] }
                  id="time_sleep"
                  size="10"
                  type="text"
                  name="timeSleepValue"
                  value={ timeSleepValue }
                  onChange={ this.handleChange }
                  onDoubleClick={ this.setControl }
                />
              </label>
            </div>
          </div>
        </div>
      );
    }
    if (activetab === 4) {
      const formGroupDInlineBlock = [spectre['form-group'], spectre['d-inline-block']].join(' ');
      return (
        <div>
          <div className={ formGroupDInlineBlock }>
            <label className={ spectre['form-label'] } htmlFor="preset_name">
              Preset Name
              <input className={ spectre['form-input'] } defaultValue="" id="preset_name" size="15" type="text" onChange={ this.saveSaver } />
            </label>
          </div>
          <div className={ formGroupDInlineBlock }>
            <label className={ spectre['form-label'] } htmlFor="saver_select">
              Load Preset
              { this.renderSelect() }
            </label>
          </div>
        </div>
      );
    }
    if (activetab === 5) {
      return (
        <Graphs />
      );
    }
    return null;
  };

  render() {
    const { controls } = this.props;
    const {
      activetab,
      onOffValue,
      senseValue,
      locRemValue
    } = this.state;
    const tabItem = spectre['tab-item'];
    const tabItemActive = [spectre['tab-item'], spectre.active].join(' ');
    return (
      <div id='c-controls' className={ [styled.container, spectre['c-controls']].join(' ') }>
        <div className={ spectre.columns }>
          <div className={ [spectre.column, spectre['col-8']].join(' ') }>
            <div className={ spectre['form-group'] }>
              <textarea className={ spectre['form-input'] } id="informer" cols="50" rows="15"></textarea>
            </div>
          </div>
          <div className={ [spectre.column, spectre['col-4']].join(' ') }>
            <label className={ spectre['form-switch'] } htmlFor="on_off">
              <input
                type="checkbox"
                id="on_off"
                name="onOffValue"
                checked={ onOffValue }
                onChange={ this.setControl }
              />
              <i className={ spectre['form-icon'] }></i><span>{ controls.on_off ? 'On' : 'Off' }</span>
            </label>
            <label className={ spectre['form-switch'] } htmlFor="sense">
              <input
                type="checkbox"
                id="sense"
                name="senseValue"
                checked={ senseValue }
                onChange={ this.setControl }
              />
              <i className={ spectre['form-icon'] }></i><span>{ controls.sense ? 'Remote Sense On' : 'Remote Sense Off' }</span>
            </label>
            <label className={ spectre['form-switch'] } htmlFor="loc_rem">
              <input
                type="checkbox"
                id="loc_rem"
                name="locRemValue"
                checked={ locRemValue }
                onChange={ this.setControl }
              />
              <i className={ spectre['form-icon'] }></i><span>{ controls.loc_rem ? 'Remote Control' : 'Local Control' }</span>
            </label>
          </div>
        </div>
        <ul className={ [spectre.tab, spectre['tab-block']].join(' ') } role="menu">
          <div className={ activetab === 1 ? `${ tabItemActive }` : `${ tabItem }` } onClick={() => this.tabChange(1)} role="menuitem" onKeyPress={() => {}} tabIndex={-1}>
            <span>Settings</span>
          </div>
          <div className={activetab === 2 ? `${ tabItemActive }` : `${ tabItem }` } onClick={() => this.tabChange(2)} role="menuitem" onKeyPress={() => {}} tabIndex={-1}>
            <span>Parameters</span>
          </div>
          <div className={activetab === 3 ? `${ tabItemActive }` : `${ tabItem }` } onClick={() => this.tabChange(3)} role="menuitem" onKeyPress={() => {}} tabIndex={-1}>
            <span>Calculation</span>
          </div>
          <div className={activetab === 4 ? `${ tabItemActive }` : `${ tabItem }` } onClick={() => this.tabChange(4)} role="menuitem" onKeyPress={() => {}} tabIndex={-1}>
            <span>Create a preset</span>
          </div>
          <div className={activetab === 5 ? `${ tabItemActive }` : `${ tabItem }` } onClick={() => this.tabChange(5)} role="menuitem" onKeyPress={() => {}} tabIndex={-1}>
            <span>View Results</span>
          </div>
        </ul>
        <div>
          { this.renderTab() }
        </div>
      </div>
    );
  }
}

const controlsShape = {
  port: PropTypes.number,
  baudrate: PropTypes.number,
  on_off: PropTypes.bool,
  sense: PropTypes.bool,
  loc_rem: PropTypes.bool,
  type: PropTypes.string,
  cc_current: PropTypes.number,
  cv_voltage: PropTypes.number,
  cw_power: PropTypes.number,
  cr_resistance: PropTypes.number,
  max_current: PropTypes.number,
  max_voltage: PropTypes.number,
  max_power: PropTypes.number,
  power_density: PropTypes.number,
  dim_power: PropTypes.string,
  area_of_cell: PropTypes.number,
  dim_area: PropTypes.string,
  min_value: PropTypes.number,
  factor: PropTypes.number,
  quantity_of_dots: PropTypes.number,
  time_sleep: PropTypes.number
};

Controls.propTypes = {
  controls: PropTypes.shape(controlsShape).isRequired,
  savers: PropTypes.arrayOf(PropTypes.string).isRequired
};

const mapStateToProps = function mapStateToProps(store) {
  return {
    controls: store.controlState.controls,
    savers: store.saverState.savers
  };
};

export default connect(mapStateToProps)(Controls);
