import { HTTP } from './commonPscEth';
import { measureSuccess, getSourceSuccess, setSourceSuccess } from '../actions/index';
import store from '../store/index';

export function measure (type) {
  return HTTP.post('/parameters/measure/', type)
    .then(response => {
      store.dispatch(measureSuccess(type, response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function getSource (type) {
  return HTTP.post('/parameters/get_source/', type)
    .then(response => {
      store.dispatch(getSourceSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function setSource (jsonstring) {
  return HTTP.post('/parameters/set_source/', jsonstring)
    .then(response => {
      store.dispatch(setSourceSuccess(jsonstring.type, response.data));
      return response.data;
    })
    .catch(error => { return error; });
}
