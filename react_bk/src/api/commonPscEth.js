import axios from 'axios';

export const HTTP = axios.create({
  baseURL: 'http://localhost:8000/psceth/api/v1/'
});

export { HTTP as default };
