import { HTTP } from './common';
import { saveSaverSuccess, loadSaverSuccess, getSaversSuccess, getControlsSuccess } from '../actions/index';
import store from '../store/index';

export function saveSaver (jsonstring) {
  return HTTP.post('/save_saver/', jsonstring)
    .then(response => {
      store.dispatch(saveSaverSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function loadSaver (saver) {
  return HTTP.post('/load_saver/', saver)
    .then(response => {
      const { fields } = JSON.parse(response.data)[0];
      store.dispatch(loadSaverSuccess(fields));
      store.dispatch(getControlsSuccess(fields));
      return fields;
    })
    .catch(error => { return error; });
}

export function getSavers () {
  return HTTP.get('/get_saver_list/').then(response => {
    store.dispatch(getSaversSuccess(response.data));
    return response.data;
  });
}
