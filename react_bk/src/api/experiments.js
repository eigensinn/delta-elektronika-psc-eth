import { HTTP } from './common';
import { createExpSuccess, getExperimentsSuccess, getExperimentSuccess, getResultSuccess } from '../actions/index';
import store from '../store/index';

export function createExp (expName) {
  return HTTP.post('/experiments/create_exp/', expName)
    .then(response => {
      store.dispatch(createExpSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function getExperiments () {
  return HTTP.get('/experiments/get_exp_list/').then(response => {
    store.dispatch(getExperimentsSuccess(response.data));
    return response.data;
  });
}

export function getExperiment (expName) {
  return HTTP.post('/experiments/get_experiment/', expName)
    .then(response => {
      store.dispatch(getExperimentSuccess(JSON.parse(response.data)));
      return JSON.parse(response.data);
    })
    .catch(error => { return error; });
}

export function getResult (resultName) {
  return HTTP.post('/results/get_result/', resultName)
    .then(response => {
      store.dispatch(getResultSuccess(JSON.parse(response.data)));
      return JSON.parse(response.data);
    })
    .catch(error => { return error; });
}
