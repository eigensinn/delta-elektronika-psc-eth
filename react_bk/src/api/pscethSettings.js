import { HTTP } from './commonPscEth';
import { getPwdStatusSuccess, setRangeSuccess, getRangesSuccess } from '../actions/index';
import store from '../store/index';

export function savePwd (pwdPair) {
  return HTTP.post('/settings/save_pwd/', pwdPair)
    .then(response => {
      return response.data;
    })
    .catch(error => { return error; });
}

export function getPwdStatus () {
  return HTTP.get('/settings/get_pwd_status/')
    .then(response => {
      store.dispatch(getPwdStatusSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function getRanges () {
  return HTTP.get('/settings/get_ranges/')
    .then(response => {
      store.dispatch(getRangesSuccess(JSON.parse(response.data)[0].fields));
      return response.data;
    })
    .catch(error => { return error; });
}

export function setRange (pair) {
  return HTTP.post('/settings/set_range/', pair)
    .then(response => {
      store.dispatch(setRangeSuccess(pair.type, response.data));
      return response.data;
    })
    .catch(error => { return error; });
}
