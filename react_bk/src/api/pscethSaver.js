import { HTTP } from './commonPscEth';
import { savePresetSuccess, delPresetSuccess, getPresetListSuccess } from '../actions/index';
import store from '../store/index';

export function savePreset (presetName) {
  return HTTP.post('/preset/save_preset/', presetName)
    .then(response => {
      store.dispatch(savePresetSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function delPreset (presetName) {
  return HTTP.post('/preset/del_preset/', presetName)
    .then(response => {
      store.dispatch(delPresetSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function getPresetList () {
  return HTTP.get('/preset/get_preset_list/')
    .then(response => {
      store.dispatch(getPresetListSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}
