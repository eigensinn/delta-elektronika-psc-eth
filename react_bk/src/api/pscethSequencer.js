import { HTTP } from './commonPscEth';
import { getSelectedSequenceNameSuccess, getSequencerDataSuccess, readSequenceModeSuccess, setSequenceModeSuccess } from '../actions/index';
import store from '../store/index';

export function getSelectedSequenceName () {
  return HTTP.get('/sequencer/get_selected_sequence_name/')
    .then(response => {
      store.dispatch(getSelectedSequenceNameSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function getSequencerData () {
  return HTTP.get('/sequencer/get_sequencer_data/')
    .then(response => {
      store.dispatch(getSequencerDataSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function triggerAStep () {
  return HTTP.get('/sequencer/trigger_a_step/')
    .then(response => {
      return response.data;
    })
    .catch(error => { return error; });
}

export function readSequenceMode () {
  return HTTP.get('/sequencer/read_sequence_mode/')
    .then(response => {
      store.dispatch(readSequenceModeSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function setSequenceMode (type) {
  return HTTP.post('/sequencer/set_sequence_mode/', type)
    .then(response => {
      store.dispatch(setSequenceModeSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}
