import { HTTP } from './commonPscEth';
import { setPudSuccess, getPudSuccess } from '../actions/index';
import store from '../store/index';

export function getIdn () {
  return HTTP.get('/base/get_idn/')
    .then(response => { return response.data; })
    .catch(error => { return error; });
}

export function saveAll (pwd) {
  return HTTP.post('/base/save_all/', pwd)
    .then(response => { return response.data; })
    .catch(error => { return error; });
}

export function getLastError () {
  return HTTP.get('/base/get_last_error/')
    .then(response => { return response.data; })
    .catch(error => { return error; });
}

export function getPud () {
  return HTTP.get('/base/get_pud/')
    .then(response => {
      store.dispatch(getPudSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function setPud (pud) {
  return HTTP.get('/base/set_pud/', pud)
    .then(response => {
      store.dispatch(setPudSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}
