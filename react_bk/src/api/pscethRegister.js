import { HTTP } from './commonPscEth';
import { getEventStatusRegisterSuccess, getStatusByteSuccess, setRegisterSuccess, getRegisterSuccess } from '../actions/index';
import store from '../store/index';

export function getEventStatusRegister () {
  return HTTP.get('/register/get_event_status_register/')
    .then(response => {
      store.dispatch(getEventStatusRegisterSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function getStatusByte () {
  return HTTP.get('/register/get_status_byte/')
    .then(response => {
      store.dispatch(getStatusByteSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function setRegister (register) {
  return HTTP.post('/register/set_register/', register)
    .then(response => {
      store.dispatch(setRegisterSuccess(register, response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function getRegister (register) {
  return HTTP.get('/register/get_register/', register)
    .then(response => {
      store.dispatch(getRegisterSuccess(register, response.data));
      return response.data;
    })
    .catch(error => { return error; });
}
