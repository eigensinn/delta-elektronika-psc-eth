import { HTTP } from './commonPscEth';
import { uploadSequenceSuccess, downloadSequenceSuccess, createSequenceSuccess, getCatalogSuccess } from '../actions/index';
import store from '../store/index';

export function uploadSequence (jsonstring) {
  return HTTP.post('/catalog/upload_sequence/', jsonstring)
    .then(response => {
      store.dispatch(uploadSequenceSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function downloadSequence (sequence) {
  return HTTP.post('/catalog/download_sequence/', sequence)
    .then(response => {
      store.dispatch(downloadSequenceSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function createSequence () {
  return HTTP.post('/catalog/create_sequence/')
    .then(response => {
      store.dispatch(createSequenceSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function getCatalog () {
  return HTTP.get('/catalog/get_catalog/')
    .then(response => {
      store.dispatch(getCatalogSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function delCatalog () {
  return HTTP.get('/catalog/del_catalog/')
    .then(response => {
      return response.data;
    })
    .catch(error => { return error; });
}
