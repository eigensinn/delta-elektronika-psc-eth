import { HTTP } from './commonPscEth';
import { getCalibrationSuccess, calibrateSuccess } from '../actions/index';
import store from '../store/index';

export function getCalibration () {
  return HTTP.get('/calibration/get_calibration/')
    .then(response => {
      store.dispatch(getCalibrationSuccess(response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function calibrate (jsonstring) {
  return HTTP.post('/calibration/calibrate/', jsonstring)
    .then(response => {
      store.dispatch(calibrateSuccess(jsonstring.type, response.data));
      return response.data;
    })
    .catch(error => { return error; });
}
