import { HTTP } from './common';
import { setControlSuccess, getControlsSuccess } from '../actions/index';
import store from '../store/index';

export function setControl (jsonstring) {
  return HTTP.post('/controls/set_control/', jsonstring)
    .then(response => {
      store.dispatch(setControlSuccess(jsonstring, response.data));
      return response.data;
    })
    .catch(error => { return error; });
}

export function run (jsonstring) {
  return HTTP.post('/controls/run/', jsonstring)
    .then(response => { return response.data; })
    .catch(error => { return error; });
}

export function getControls () {
  return HTTP.get('/controls/get_control_states/').then(response => {
    store.dispatch(getControlsSuccess(JSON.parse(response.data)[0].fields));
  });
}
