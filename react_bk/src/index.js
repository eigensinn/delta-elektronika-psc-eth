import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { SnackbarProvider } from 'notistack';

import store from './store';
import App from './components/App';

ReactDOM.render(
  <BrowserRouter>
    <Fragment>
      <Provider store={store}>
        <SnackbarProvider maxSnack={3}>
          <App />
        </SnackbarProvider>
      </Provider>
    </Fragment>
  </BrowserRouter>,
  document.getElementById('root')
);
