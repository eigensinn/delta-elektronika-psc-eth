import { GET_SELECTED_SEQUENCE_NAME, GET_SEQUENCER_DATA, READ_SEQUENCE_MODE, SET_SEQUENCE_MODE } from '../constants/ActionTypes';

const initialState = {
  seqData: {
    selected_sequence: 'None',
    seq_for_upload: ''
  },
  seqMode: 'STOP'
};

export default function pscEthSequencerReducer (state = initialState, action) {
  switch (action.type) {
    
  case GET_SELECTED_SEQUENCE_NAME:
    return {
      ...state,
      seqData: {
        ...state.seqData,
        selected_sequence: action.selected
      }
    };
    
  case GET_SEQUENCER_DATA:
    return {
      ...state,
      seqData: action.json
    };
    
  case READ_SEQUENCE_MODE:
    return {
      ...state,
      seqMode: action.mode
    };
    
  case SET_SEQUENCE_MODE:
    return {
      ...state,
      seqMode: action.mode
    };

  default:
    return state;
  }
}
