import { SAVE_PRESET, DEL_PRESET, GET_PRESET_LIST } from '../constants/ActionTypes';

const initialState = {
  presets: ["None"]
};

export default function pscEthRegisterReducer (state = initialState, action) {
  switch (action.type) {
    
  case SAVE_PRESET:
    return {
      presets: [action.presetName, ...state.presets]
    };
    
  case DEL_PRESET:
    return {
      presets: action.presets
    };
    
  case GET_PRESET_LIST:
    return {
      presets: action.presets
    };

  default:
    return state;
  }
}
