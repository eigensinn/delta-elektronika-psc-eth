import { GET_EVENT_STATUS_REGISTER, GET_STATUS_BYTE, SET_REGISTER, GET_REGISTER } from '../constants/ActionTypes';

const initialState = {
  eventStatusRegister: 0,
  statusByte: 0,
  registerData: {
    event_status_enable: 0,
    service_request_enable: 0,
    positive_transition_register: 0,
    negative_transition_register: 0,
    enable_state: 0
  }
};

export default function pscEthRegisterReducer (state = initialState, action) {
  switch (action.type) {
    
  case GET_EVENT_STATUS_REGISTER:
    return {
      ...state,
      eventStatusRegister: action.evtStatus
    };
    
  case GET_STATUS_BYTE:
    return {
      ...state,
      eventStatusRegister: action.statusByte
    };
    
  case SET_REGISTER:
    return {
      ...state,
      registerData: {
        ...state.registerData,
        [action.meta]: action.value
      }
    };
    
  case GET_REGISTER:
    return {
      ...state,
      registerData: {
        ...state.registerData,
        [action.meta]: action.value
      }
    };

  default:
    return state;
  }
}
