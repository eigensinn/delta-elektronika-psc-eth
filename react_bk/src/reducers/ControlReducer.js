import * as types from '../constants/ActionTypes';

const initialState = {
  controls: {
    port: 4,
    baudrate: 9600,
    on_off: false,
    sense: false,
    loc_rem: false,
    type: "on_off",
    cc_current: 0,
    cv_voltage: 0,
    cw_power: 0,
    cr_resistance: 0,
    max_current: 0,
    max_voltage: 0,
    max_power: 0,
    power_density: 0,
    dim_power: "W/cm2",
    area_of_cell: 0,
    dim_area: "cm2",
    min_value: 0,
    factor: 0,
    quantity_of_dots: 0,
    time_sleep: 0
  }
};

export default function ControlReducer (state = initialState, action) {
  switch (action.type) {

  case types.GET_CONTROLS:
    return {
      ...state,
      controls: action.fields
    };

  case types.PORT_CONTROL:
  case types.BAUD_CONTROL:
  case types.ON_OFF_CONTROL:
  case types.SENSE_CONTROL:
  case types.LOC_REM_CONTROL:
  case types.MODE_CONTROL:
  case types.CC_CURRENT_CONTROL:
  case types.CV_VOLTAGE_CONTROL:
  case types.CW_POWER_CONTROL:
  case types.CR_RESISTANCE_CONTROL:
  case types.MAX_VOLTAGE_CONTROL:
  case types.MAX_CURRENT_CONTROL:
  case types.MAX_POWER_CONTROL:
  case types.POWER_DENSITY_CONTROL:
  case types.AREA_CONTROL:
  case types.MIN_VALUE_CONTROL:
  case types.FACTOR_CONTROL:
  case types.QUANTITY_CONTROL:
  case types.TIME_SLEEP_CONTROL:
    return {
      ...state,
      controls: {
        ...state.controls,
        [action.meta]: action.response
      }
    };

  case types.DIM_POWER:
    return {
      ...state,
      controls: {
        ...state.controls,
        [action.meta]: action.response,
        dim_area: action.response.split('/').pop()
      }
    };

  case types.DIM_AREA:
    return {
      ...state,
      controls: {
        ...state.controls,
        [action.meta]: action.response,
        dim_power: `W/${action.response}`
      }
    };
    
  default:
    return state;
  }
}
