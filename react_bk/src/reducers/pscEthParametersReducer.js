import { MEASURE, GET_SOURCE, SET_SOURCE } from '../constants/ActionTypes';

const initialState = {
  measureData: {
    voltage: 0,
    current: 0,
    power: 0
  },
  sourceData: {
    output_voltage: 0,
    output_current: 0,
    max_voltage: 0,
    max_current: 0
  }
};

export default function pscEthParametersReducer (state = initialState, action) {
  switch (action.type) {
    
  case MEASURE:
    return {
      ...state,
      measureData: {
        ...state.measureData,
        [action.meta]: action.value
      }
    };
    
  case GET_SOURCE:
    return {
      ...state,
      sourceData: {
        ...state.sourceData,
        [action.meta]: action.value
      }
    };
    
  case SET_SOURCE:
    return {
      ...state,
      sourceData: {
        ...state.sourceData,
        [action.meta]: action.value
      }
    };

  default:
    return state;
  }
}
