import { CREATE_EXP, GET_EXPERIMENTS, GET_EXPERIMENT, GET_RESULT } from '../constants/ActionTypes';

const initialState = {
  experiments: {
    "None": ["res_1"]
  },
  resultData: [[1, 1]],
  expData: [
    {name: "None", data: []}
  ]
};

export default function ExpReducer (state = initialState, action) {
  switch (action.type) {

  case CREATE_EXP:
    return {
      ...state,
      experiments: {
        ...state.experiments,
        [action.expName]: []
      }
    };
    
  case GET_EXPERIMENTS:
    return {
      ...state,
      experiments: action.experiments
    };
    
  case GET_EXPERIMENT:
    return {
      ...state,
      expData: action.expData
    };
    
  case GET_RESULT:
    return {
      ...state,
      resultData: action.resultData
    };

  default:
    return state;
  }
}
