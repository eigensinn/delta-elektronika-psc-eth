import { GET_CALIBRATION, CALIBRATE } from '../constants/ActionTypes';

const initialState = {
  calibrationData: {
    gain_source_voltage: 0,
    gain_source_current: 0,
    offset_source_voltage: 0,
    offset_source_current: 0,
    gain_measure_voltage: 0,
    gain_measure_current: 0,
    offset_measure_voltage: 0,
    offset_measure_current: 0
  }
};

export default function pscEthCalibrationReducer (state = initialState, action) {
  switch (action.type) {
    
  case GET_CALIBRATION:
    return {
      ...state,
      calibrationData: action.json
    };
    
  case CALIBRATE:
    return {
      ...state,
      calibrationData: {
        ...state.calibrationData,
        [action.meta]: action.response
      }
    };

  default:
    return state;
  }
}
