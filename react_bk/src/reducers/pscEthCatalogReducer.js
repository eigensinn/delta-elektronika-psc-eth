import { UPLOAD_SEQUENCE, DOWNLOAD_SEQUENCE, CREATE_SEQUENCE, GET_CATALOG } from '../constants/ActionTypes';

const initialState = {
  uploadedSeq: '',
  downloadedSeq: '',
  seqName: '',
  seqList: ['None', 'None1']
};

export default function pscEthCatalogReducer (state = initialState, action) {
  switch (action.type) {
    
  case UPLOAD_SEQUENCE:
    return {
      ...state,
      uploadedSeq: action.stepBody
    };
    
  case DOWNLOAD_SEQUENCE:
    return {
      ...state,
      downloadedSeq: action.downloadedSeq
    };
    
  case CREATE_SEQUENCE:
    return {
      ...state,
      seqName: action.selected
    };
    
  case GET_CATALOG:
    return {
      ...state,
      seqList: action.seqList
    };

  default:
    return state;
  }
}
