import { GET_PUD, SET_PUD } from '../constants/ActionTypes';

const initialState = {
  pud: "None"
};

export default function pscEthBaseReducer (state = initialState, action) {
  switch (action.type) {
    
  case GET_PUD:
    return {
      ...state,
      pud: action.pud
    };
    
  case SET_PUD:
    return {
      ...state,
      pud: action.pud
    };

  default:
    return state;
  }
}
