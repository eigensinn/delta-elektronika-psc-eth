import { SAVE_SAVER, LOAD_SAVER, GET_SAVERS } from '../constants/ActionTypes';

const initialState = {
  savers: ["None"], // список сохранений
  controls: {
    port: 4,
    baudrate: 9600,
    on_off: false,
    sense: false,
    loc_rem: false,
    type: "on_off",
    cc_current: 0,
    cv_voltage: 0,
    cw_power: 0,
    cr_resistance: 0,
    max_current: 0,
    max_voltage: 0,
    max_power: 0,
    power_density: 0,
    dim_power: "W/cm2",
    area_of_cell: 0,
    dim_area: "cm2",
    min_value: 0,
    factor: 0,
    quantity_of_dots: 0,
    time_sleep: 0
  }
};

export default function SaverReducer (state = initialState, action) {
  switch (action.type) {

  case SAVE_SAVER:
    return {
      savers: [action.saver, ...state.savers]
    };

  case LOAD_SAVER:
    return {
      savers: state.savers
    };
    
  case GET_SAVERS:
    return {
      savers: action.savers
    };

  default:
    return state;
  }
}
