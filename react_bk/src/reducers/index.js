import { combineReducers } from 'redux';

// Reducers
import SaverReducer from './SaverReducer';
import ControlReducer from './ControlReducer';
import ExpReducer from './ExpReducer';
import pscEthBaseReducer from './pscEthBaseReducer';
import pscEthCalibrationReducer from './pscEthCalibrationReducer';
import pscEthParametersReducer from './pscEthParametersReducer';
import pscEthSettingsReducer from './pscEthSettingsReducer';
import pscEthCatalogReducer from './pscEthCatalogReducer';
import pscEthRegisterReducer from './pscEthRegisterReducer';
import pscEthSaverReducer from './pscEthSaverReducer';
import pscEthSequencerReducer from './pscEthSequencerReducer';

// Combine Reducers
const reducers = combineReducers({
  saverState: SaverReducer,
  controlState: ControlReducer,
  expState: ExpReducer,
  pscEthBaseState: pscEthBaseReducer,
  pscEthCalibrationState: pscEthCalibrationReducer,
  pscEthParametersState: pscEthParametersReducer,
  pscEthSettingsState: pscEthSettingsReducer,
  pscEthCatalogState: pscEthCatalogReducer,
  pscEthRegisterState: pscEthRegisterReducer,
  pscEthSaverState: pscEthSaverReducer,
  pscEthSequencerState: pscEthSequencerReducer
});

export default reducers;
