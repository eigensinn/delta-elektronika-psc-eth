import { PWD_STATUS, SET_RANGE, GET_RANGES } from '../constants/ActionTypes';

const initialState = {
  pwdStatus: 0,
  range: {
    rng_output_voltage: 100,
    rng_output_current: 100,
    rng_max_voltage: 100,
    rng_max_current: 100,
    rng_gain_source_voltage: 100,
    rng_gain_source_current: 100,
    rng_offset_source_voltage: 100,
    rng_offset_source_current: 100,
    rng_gain_measure_voltage: 100,
    rng_gain_measure_current: 100,
    rng_offset_measure_voltage: 100,
    rng_offset_measure_current: 100
  }
};

export default function pscEthSettingsReducer (state = initialState, action) {
  switch (action.type) {
    
  case PWD_STATUS:
    return {
      ...state,
      pwdStatus: action.pwdStatus
    };
    
  case SET_RANGE:
    return {
      ...state,
      range: {
        ...state.range,
        [action.meta]: action.value
      }
    };
    
  case GET_RANGES:
    return {
      ...state,
      range: action.jsonstring
    };

  default:
    return state;
  }
}
