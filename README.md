# DELTA ELEKTRONIKA BV power supply control with PSC-ETH

  * Based on Django 2.1.7;
  * Use DjangoRestFramework;
  * Use PostgreSQL >=9.6;
  * Use React & Redux;
  * Use Spectre.css

## PostgreSQL install

### Ubuntu

Download [Shell script](https://salsa.debian.org/postgresql/postgresql-common/raw/master/pgdg/apt.postgresql.org.sh)

    user@machine:~$ chmod +x /path/to/apt.postgresql.org.sh
    user@machine:~$ /path/to/apt.postgresql.org.sh
    user@machine:~$ sudo apt-get install postgresql-11 pgadmin4
    user@machine:~$ sudo -u postgres psql
    postgres=# ALTER USER postgres PASSWORD '123456';
    postgres=# \q

in pgAdmin4:

    Servers -> Create - Server
    General -> Name = localserver
    Connection -> Host = 127.0.0.1
    Port = 5432
    Maintenance database = postgres
    Username = postgres
    Password = 123456

## React install & run

### Windows

install nodejs

    npm install -g webpack
    cd react_bk
    npm install
    npm start (in development mode) or npm run build (in production mode)

### Ubuntu
    sudo snap install node --channel=10/stable --classic
    sudo npm install -g webpack
    cd react_bk
    npm install
    npm start (in development mode) or npm run build (in production mode)

## Run application

Set your database password to [settings.py](https://bitbucket.org/eigensinn/delta-elektronika-psc-eth/src/master/DjangoPrecision/settings.py):

    DATABASES['default']['PASSWORD']

Make migrations to sync database with [0001_initial.py](https://bitbucket.org/eigensinn/delta-elektronika-psc-eth/src/master/bk/migrations/0001_initial.py) and [0001_initial.py](https://bitbucket.org/eigensinn/delta-elektronika-psc-eth/src/master/psceth/migrations/0001_initial.py):

    cd path\to\manage.py
    venv\Scripts\activate
    python manage.py migrate

Create superuser:

    python manage.py createsuperuser --username=admin --email=joe@example.com

Run development server:

    python manage.py runserver

Then run browser and type to enter [admin](https://bitbucket.org/eigensinn/delta-elektronika-psc-eth/src/master/screenshots/admin.png):

    http://127.0.0.1:8000/admin/

or type to enter [BK Precision 8500 series control interface](https://bitbucket.org/eigensinn/delta-elektronika-psc-eth/src/master/screenshots/bk_precision_8500_control.png):

    http://127.0.0.1:8080/

or type to enter [PSC-ETH control interface](https://bitbucket.org/eigensinn/delta-elektronika-psc-eth/src/master/screenshots/psc_eth_interface.png)

    http://127.0.0.1:8000/psceth-home/

## Links and thanks

To interact with the BK Precision Series 8500 electronic load, a modified library [dcload.py](https://bitbucket.org/eigensinn/delta-elektronika-psc-eth/src/master/dcload.py), rewritten for Python >= 3.3. The original library for Python <= 2.7 can be downloaded from the [PyLib85xx.zip](http://www.bkprecision.com/products/dc-electronic-loads/8500-300-w-programmable-dc-electronic-load.html) on the official website of BK Precision.

## License

[MIT License](https://bitbucket.org/eigensinn/delta-elektronika-psc-eth/src/master/LICENSE)