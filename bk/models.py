from django.db import models
from django.utils.translation import ugettext_lazy as _
from jsonfield.fields import JSONField


class LaserScanExperiment(models.Model):
    experiment_name = models.CharField(max_length=200, verbose_name=_('Experiment name'))


class LaserScanResult(models.Model):
    results = models.ForeignKey(LaserScanExperiment, on_delete=models.CASCADE)
    result_name = models.CharField(max_length=200, verbose_name=_('Result name'))
    image = models.ImageField(upload_to='LasScan/%Y/%m/%d',
                              height_field='ImageHeight',
                              width_field='ImageWidth',
                              blank=True,
                              null=True,
                              verbose_name=_('image'))
    ImageWidth = models.IntegerField(blank=True, null=True, verbose_name=_('width'))
    ImageHeight = models.IntegerField(blank=True, null=True, verbose_name=_('height'))
    ADCRangeCode = models.IntegerField(blank=True, null=True, verbose_name=_('ADC Range Code'))
    VerticalResolution = models.CharField(max_length=200, verbose_name=_('Vertical Resolution'))
    HorizontalResolution = models.CharField(max_length=200, verbose_name=_('Horizontal Resolution'))
    ScanStartedAt = models.CharField(max_length=200, verbose_name=_('Scan Started At'))


class Saver(models.Model):
    saver_name = models.CharField(max_length=200, verbose_name=_('Saver name'))
    preset = JSONField(default='{}', verbose_name=_('preset'))


class SolarExperiment(models.Model):
    solar_exp_name = models.CharField(max_length=200, verbose_name=_('experiment name'))

    def __unicode__(self):
        return self.solar_exp_name


class SolarResult(models.Model):
    results = models.ForeignKey(SolarExperiment, models.CASCADE)
    result_name = models.CharField(max_length=200, verbose_name=_('result name'))
    product_information = JSONField(verbose_name=_('product information'))
    date = JSONField(verbose_name=_('date'))
    area = JSONField(verbose_name=_('area'))
    efficiency = models.FloatField(verbose_name=_('Efficiency'))
    graph = models.ImageField(upload_to='Solar/%Y/%m/%d',
                              height_field='graph_height',
                              width_field='graph_width',
                              blank=True,
                              null=True,
                              verbose_name=_('image'))
    graph_width = models.IntegerField(blank=True, null=True, verbose_name=_('width'))
    graph_height = models.IntegerField(blank=True, null=True, verbose_name=_('height'))
    data = models.CharField(max_length=100000, verbose_name=_('Data'))
    data_for_graph = JSONField(verbose_name=_('Data for Graph'))
    max_power_point = JSONField(verbose_name=_('Max Power Point'))


class Control(models.Model):
    port = models.IntegerField(default=4)
    baudrate = models.IntegerField(default=9600)
    on_off = models.BooleanField(default=False)
    sense = models.BooleanField(default=False)
    loc_rem = models.BooleanField(default=False)
    type = models.CharField(max_length=50, default='on_off')
    mode = models.CharField(max_length=2, default='cv')
    cc_current = models.FloatField(default=0.0)
    cv_voltage = models.FloatField(default=0.0)
    cw_power = models.FloatField(default=0.0)
    cr_resistance = models.FloatField(default=0.0)
    max_current = models.FloatField(default=0.0)
    max_voltage = models.FloatField(default=0.0)
    max_power = models.FloatField(default=0.0)
    power_density = models.FloatField(default=0.0)
    dim_power = models.CharField(max_length=10, default='W/cm2')
    area_of_cell = models.FloatField(default=0.0)
    dim_area = models.CharField(max_length=10, default='cm2')
    min_value = models.FloatField(default=0.1)
    factor = models.FloatField(default=0.1)
    quantity_of_dots = models.IntegerField(default=50)
    time_sleep = models.FloatField(default=0.1)
