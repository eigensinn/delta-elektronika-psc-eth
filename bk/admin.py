from bk.models import LaserScanExperiment, LaserScanResult, Saver, SolarResult, SolarExperiment
from psceth.models import PscEthSaver
from django.contrib import admin
from django.utils.safestring import mark_safe
from easy_thumbnails.files import get_thumbnailer
from django import forms


class AdminImageWidget(forms.FileInput):
    """
    A ImageField Widget for admin that shows a thumbnail.
    """
    def __init__(self, attrs={}):
        super(AdminImageWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        output = []
        if value and hasattr(value, "url"):
            thumbnailer = get_thumbnailer(value)
            thumbnail_options = {'crop': True}
            thumbnail_options.update({'size': (120, 120)})
            thumb = thumbnailer.get_thumbnail(thumbnail_options)
            output.append(('<a class="boxer" href="%s"><img src="%s"></a>' % (value.url, thumb.url)))
        output.append(super(AdminImageWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))


class ResultImageForm(forms.ModelForm):
    class Meta:
        model = LaserScanResult
        widgets = {
            'image': AdminImageWidget,
        }
        fields = "__all__"


class SolarResultGraphForm(forms.ModelForm):
    class Meta:
        model = SolarResult
        widgets = {
            'graph': AdminImageWidget,
        }
        fields = "__all__"


class ResultInline(admin.TabularInline):
    model = LaserScanResult
    extra = 3
    readonly_fields = ['ImageWidth', 'ImageHeight']
    form = ResultImageForm


class SolarResultInline(admin.TabularInline):
    model = SolarResult
    extra = 3
    readonly_fields = ['graph_width', 'graph_height']
    form = SolarResultGraphForm


class LasScanExperimentAdmin(admin.ModelAdmin):
    inlines = [ResultInline]
    list_display = ['experiment_name']
    search_fields = ['experiment_name']


class SolarExperimentAdmin(admin.ModelAdmin):
    inlines = [SolarResultInline]
    list_display = ['solar_exp_name']
    search_fields = ['solar_exp_name']


class SaverAdmin(admin.ModelAdmin):
    list_display = ['saver_name', 'preset']


class PscEthSaverAdmin(admin.ModelAdmin):
    list_display = ['saver_name', 'parameters']

 
admin.site.register(LaserScanExperiment, LasScanExperimentAdmin)
admin.site.register(SolarExperiment, SolarExperimentAdmin)
admin.site.register(Saver, SaverAdmin)
admin.site.register(PscEthSaver, PscEthSaverAdmin)
