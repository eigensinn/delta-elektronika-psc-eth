from django.contrib import admin
from django.conf.urls import include, url
from DjangoPrecision import views as djangoprecision_views

admin.autodiscover()

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^bk/', include('bk.urls')),
    url(r'^psceth/', include('psceth.urls')),
    url('^$', djangoprecision_views.homepage),
    url(r'^psceth-home/', djangoprecision_views.psceth_home),
    url(r'^saver/(?P<saver_name>\w+)/$', djangoprecision_views.SaverListView),
    url(r'^psceth-saver/(?P<saver_name>\w+)/$', djangoprecision_views.PscEthSaverListView)]
