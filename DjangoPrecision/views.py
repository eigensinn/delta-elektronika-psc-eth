from django.shortcuts import render
from django.views.generic import ListView
from bk.models import Saver, SolarExperiment, SolarResult
from psceth.models import PscEthSaver
from django.shortcuts import get_object_or_404


def homepage(request):
    return render(request, 'homepage.html')


def psceth_home(request):
    return render(request, 'psceth.html')


class SaverListView(ListView):
    context_object_name = "saver_list"
    template_name = "homepage.html"

    def get_queryset(self):
        return Saver.objects.all()
    
    def get_context_data(self, **kwargs):
        context = super(SaverListView, self).get_context_data(**kwargs)
        context['current_saver'] = get_object_or_404(Saver, saver_name=self.kwargs['saver_name'])
        l_ses = []
        ses = SolarExperiment.objects.values_list("id", flat=True)
        for se in ses:
            l_rns = []
            rns = SolarResult.objects.filter(results_id=se).values_list("result_name", flat=True)
            for rn in rns:
                li = '<li><a class="result" href="#">'+rn+'</a></li>'
                l_rns.append(li)
            s_rns = "".join(l_rns)
            se_name = SolarExperiment.objects.get(id=se).solarexp_name
            s_se = '<a href="#" class="experiment">'+se_name+'</a><ul>'+s_rns+'</ul>'
            l_ses.append(s_se)
        context['solar_results'] = l_ses
        return context


class PscEthSaverListView(ListView):
    context_object_name = "saver_list"
    template_name = "psceth.html"

    def get_queryset(self):
        return PscEthSaver.objects.all()
    
    def get_context_data(self, **kwargs):
        context = super(PSCETHSaverListView, self).get_context_data(**kwargs)
        context['current_saver'] = get_object_or_404(PscEthSaver, saver_name=self.kwargs['saver_name'])
        return context