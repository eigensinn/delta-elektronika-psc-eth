$(document).ready(function(){
    $(".experiment").click(function(e) {
        e.preventDefault();
        var solarexp_name = $(this).text();
        var result_filter = $("#result_filter").val();
        if (solarexp_name != "Не выбрано") {
            //AJAX-запрос для получения странички с графиком
            $.ajax({
                type: 'GET',
                data: { "result_filter": result_filter },
                url: "http://127.0.0.1:8000/bk/multigraph/"+solarexp_name+"",
                beforeSend: function(){
                    $('#graph_content').empty()
                    $('#progressbar').show();
                },
                success: function(data){
                    $('#graph_content').append(data);
                    $('#progressbar').hide();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $('#graph_content').prepend('Данные не могут быть получены');
                    $('#progressbar').hide();
                }
            });
        }
        else {
            $('#graph_content').empty();
        }
    });
});