$(document).ready(function(){
    $(".result").click(function(e) {
        e.preventDefault();
        var result_name = $(this).text();
        //AJAX-запрос для получения странички с графиком
        $.ajax({
            type: 'GET',
            url: "http://127.0.0.1:8000/bk/graph/"+result_name+"",
            beforeSend: function(){
                $('#graph_content').empty()
                $('#progressbar').show();
            },
            success: function(data){
                $('#graph_content').append(data)
                $('#progressbar').hide();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('#graph_content').prepend('Данные не могут быть получены');
                $('#progressbar').hide();
            }
        });
    });
});